import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutContainerComponent } from './core/containers/main-layout-container/main-layout-container.component';

const children: Routes = [
  {
    path: 'pokedex',
    loadChildren: () => import('./pages/pokedex/pokedex.module').then((m) => m.PokedexModule)
  },
  {
    path: 'pokemons',
    loadChildren: () => import('./pages/pokemons/pokemons.module').then((m) => m.PokemonsModule)
  },
  {
    path: 'evolutions',
    loadChildren: () => import('./pages/evolutions/evolutions.module').then((m) => m.EvolutionsModule)
  },
  {
    path: 'types',
    loadChildren: () => import('./pages/pokemons-types/pokemons-types.module').then((m) => m.PokemonsTypesModule)
  },
  {
    path: 'regions',
    loadChildren: () => import('./pages/regions/regions.module').then((m) => m.RegionsModule)
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'pokedex'
  }
];

const routes: Routes = [
  { path: '', component: MainLayoutContainerComponent, children },
  { path: '**', redirectTo: 'pokedex' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
