import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';
import { PokemonsSearchHelper } from './store/pokemons/search/pokemons-search.helper';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  showSplashScreen = true;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private pokemonsSearchHelper: PokemonsSearchHelper,
    private scrollToService: ScrollToService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.pokemonsSearchHelper.fetchPokemonsSearch();
    this.waitForSplashScreen();

    this.router.events.subscribe((route) => {
      if (route instanceof NavigationEnd) this.scrollToService.scrollTo({ target: 'top' });
    });
  }

  private waitForSplashScreen(): void {
    if (this.document.readyState === 'complete') {
      setTimeout(() => (this.showSplashScreen = false), 2000);
    } else {
      window.addEventListener('load', () => setTimeout(() => (this.showSplashScreen = false), 2000));
    }
  }
}
