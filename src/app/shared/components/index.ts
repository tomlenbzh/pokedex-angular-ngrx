import { ListPaginatorComponent } from './list-paginator/list-paginator.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { PokemonLoaderComponent } from './pokemon-loader/pokemon-loader.component';
import { SplashScreenComponent } from './splash-screen/splash-screen.component';

export const components = [PokemonLoaderComponent, ListPaginatorComponent, SplashScreenComponent, PageHeaderComponent];
