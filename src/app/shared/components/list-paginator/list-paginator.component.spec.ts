import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../shared.module';
import { ListPaginatorComponent } from './list-paginator.component';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { setMatIcons } from 'src/app/utils/methods/set-icons';
import { HttpClientModule } from '@angular/common/http';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';

describe('ListPaginatorComponent', () => {
  let component: ListPaginatorComponent;
  let fixture: ComponentFixture<ListPaginatorComponent>;
  let domSanitizer: DomSanitizer;
  let matIconRegistry: MatIconRegistry;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListPaginatorComponent],
      imports: [SharedModule, NgxPaginationModule, HttpClientModule],
      providers: [ScrollToService]
    }).compileComponents();

    fixture = TestBed.createComponent(ListPaginatorComponent);
    component = fixture.componentInstance;

    domSanitizer = TestBed.inject(DomSanitizer);
    matIconRegistry = TestBed.inject(MatIconRegistry);
    setMatIcons(matIconRegistry, domSanitizer);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
