import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';
import { PaginationControlsDirective } from 'ngx-pagination';

@Component({
  selector: 'app-list-paginator',
  templateUrl: './list-paginator.component.html',
  styleUrls: ['./list-paginator.component.scss']
})
export class ListPaginatorComponent {
  @Input() itemsPerPage!: number;
  @Input() currentPage!: number;
  @Input() totalItems!: number;

  @Input() maxSize = 6;
  @Input() disabled = false;

  @Output() changed: EventEmitter<number> = new EventEmitter<number>();

  constructor(private scrollToService: ScrollToService) {}

  /**
   * Emits the selected page number to the parent component.
   *
   * @param     { number }      page
   */
  onPageChange(page: number): void {
    this.changed.emit(page);
    this.scrollToService.scrollTo({ target: 'top' });
  }

  /**
   * Navigates to the next page if current page is not last.
   *
   * @param     { PaginationControlsDirective }     paginator
   * @returns   { void }
   */
  next(paginator: PaginationControlsDirective): void {
    if (paginator.isLastPage()) return;
    paginator.next();
  }

  /**
   * Navigates to the previous page if current page is not first.
   *
   * @param     { PaginationControlsDirective }     paginator
   * @returns   { void }
   */
  previous(paginator: PaginationControlsDirective): void {
    if (paginator.isFirstPage()) return;
    paginator.previous();
  }

  /**
   * Sets the current page number.
   *
   * @param     { PaginationControlsDirective }     paginator
   * @returns   { void }
   */
  setCurrent(paginator: PaginationControlsDirective, value: number): void {
    paginator.setCurrent(value);
  }
}
