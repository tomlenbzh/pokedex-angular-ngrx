import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

export const MaterialModules = [
  MatIconModule,
  MatTooltipModule,
  MatButtonModule,
  MatRippleModule,
  MatCardModule,
  MatExpansionModule,
  MatToolbarModule,
  MatSidenavModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatInputModule
];
