import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModules } from './modules/material.module';
import { components } from './components';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [...components],
  imports: [CommonModule, LazyLoadImageModule, NgxPaginationModule, MaterialModules],
  exports: [MaterialModules, ...components, MaterialModules]
})
export class SharedModule {}
