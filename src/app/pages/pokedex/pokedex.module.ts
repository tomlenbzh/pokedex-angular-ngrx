import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PokedexRoutingModule } from './pokedex-routing.module';
import { components } from './components';
import { containers } from './containers';
import { SharedModule } from 'src/app/shared/shared.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  declarations: [...containers, ...components],
  imports: [CommonModule, PokedexRoutingModule, SharedModule, LazyLoadImageModule]
})
export class PokedexModule {}
