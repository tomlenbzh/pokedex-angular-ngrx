import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SharedModule } from 'src/app/shared/shared.module';
import { AboutComponent } from '../../components/about/about.component';
import { NavigationBoxComponent } from '../../components/navigation-box/navigation-box.component';
import { PokedexComponent } from '../../components/pokedex/pokedex.component';
import { WallpaperComponent } from '../../components/wallpaper/wallpaper.component';
import { PokedexContainerComponent } from './pokedex-container.component';

describe('PokedexContainerComponent', () => {
  let component: PokedexContainerComponent;
  let fixture: ComponentFixture<PokedexContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        PokedexContainerComponent,
        PokedexComponent,
        NavigationBoxComponent,
        WallpaperComponent,
        AboutComponent
      ],
      imports: [SharedModule, LazyLoadImageModule],
      providers: [ScrollToService]
    }).compileComponents();

    fixture = TestBed.createComponent(PokedexContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
