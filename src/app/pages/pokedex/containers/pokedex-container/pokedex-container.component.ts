import { Component } from '@angular/core';

@Component({
  selector: 'app-pokedex-container',
  template: '<app-pokedex></app-pokedex>'
})
export class PokedexContainerComponent {}
