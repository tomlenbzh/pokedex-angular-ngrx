import { Component } from '@angular/core';
import { ScrollToConfigOptions, ScrollToService } from '@nicky-lenaers/ngx-scroll-to';

@Component({
  selector: 'app-wallpaper',
  templateUrl: './wallpaper.component.html',
  styleUrls: ['./wallpaper.component.scss']
})
export class WallpaperComponent {
  wallpaperBackground = '/assets/images/Wallpapers/MyPokedex.png';
  angularImg = 'https://angular.io/assets/images/logos/angular/angular.svg';
  ngrxImg = 'https://ngrx.io/assets/images/badge.svg';
  pokeballImg = 'assets/images/load.png';

  constructor(private scrollToService: ScrollToService) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Scrolls to section target id.
   *
   * @param       { string }      target
   */
  scrollTo(target: string): void {
    const config: ScrollToConfigOptions = { target };
    this.scrollToService.scrollTo(config);
  }
}
