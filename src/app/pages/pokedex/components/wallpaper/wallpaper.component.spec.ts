import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SharedModule } from 'src/app/shared/shared.module';
import { WallpaperComponent } from './wallpaper.component';

describe('WallpaperComponent', () => {
  let component: WallpaperComponent;
  let fixture: ComponentFixture<WallpaperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WallpaperComponent],
      imports: [SharedModule, LazyLoadImageModule],
      providers: [ScrollToService]
    }).compileComponents();

    fixture = TestBed.createComponent(WallpaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
