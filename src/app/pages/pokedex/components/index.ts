import { AboutComponent } from './about/about.component';
import { NavigationBoxComponent } from './navigation-box/navigation-box.component';
import { PokedexComponent } from './pokedex/pokedex.component';
import { WallpaperComponent } from './wallpaper/wallpaper.component';

export const components = [PokedexComponent, WallpaperComponent, AboutComponent, NavigationBoxComponent];
