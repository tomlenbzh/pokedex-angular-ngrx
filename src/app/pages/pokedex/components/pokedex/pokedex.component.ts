import { Component } from '@angular/core';
import { HomeSections } from 'src/app/utils/constants/pokedex.constants';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.scss']
})
export class PokedexComponent {
  sections = HomeSections;
}
