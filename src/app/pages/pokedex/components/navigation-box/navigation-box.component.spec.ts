import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NavigationBoxComponent } from './navigation-box.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { SharedModule } from 'src/app/shared/shared.module';
import { setMatIcons } from 'src/app/utils/methods/set-icons';

describe('NavigationBoxComponent', () => {
  let component: NavigationBoxComponent;
  let fixture: ComponentFixture<NavigationBoxComponent>;
  let domSanitizer: DomSanitizer;
  let matIconRegistry: MatIconRegistry;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NavigationBoxComponent],
      imports: [SharedModule, HttpClientModule, RouterTestingModule, BrowserAnimationsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(NavigationBoxComponent);
    component = fixture.componentInstance;

    domSanitizer = TestBed.inject(DomSanitizer);
    matIconRegistry = TestBed.inject(MatIconRegistry);
    setMatIcons(matIconRegistry, domSanitizer);

    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
