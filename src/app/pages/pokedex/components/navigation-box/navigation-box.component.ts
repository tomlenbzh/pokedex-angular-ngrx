import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation-box',
  templateUrl: './navigation-box.component.html',
  styleUrls: ['./navigation-box.component.scss']
})
export class NavigationBoxComponent {
  @Input() section!: any;

  constructor(private router: Router) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Navigates to section url.
   */
  navigateTolink(): void {
    this.router.navigateByUrl(this.section.route);
  }
}
