import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PokemonsTypeSingleContainerComponent } from './containers/pokemons-type-single-container/pokemons-type-single-container.component';
import { PokemonsTypesContainerComponent } from './containers/pokemons-types-container/pokemons-types-container.component';

const routes: Routes = [
  { path: '', component: PokemonsTypesContainerComponent },
  { path: ':id', component: PokemonsTypeSingleContainerComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PokemonsTypesRoutingModule {}
