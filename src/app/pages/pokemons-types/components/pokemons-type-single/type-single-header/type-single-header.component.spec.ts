import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SharedModule } from 'src/app/shared/shared.module';
import { TypeSingleHeaderComponent } from './type-single-header.component';

describe('TypeSingleHeaderComponent', () => {
  let component: TypeSingleHeaderComponent;
  let fixture: ComponentFixture<TypeSingleHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TypeSingleHeaderComponent],
      imports: [SharedModule, LazyLoadImageModule]
    }).compileComponents();

    fixture = TestBed.createComponent(TypeSingleHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
