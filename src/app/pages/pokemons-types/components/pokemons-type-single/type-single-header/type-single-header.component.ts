import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ALL_TYPES } from 'src/app/utils/constants/types.constants';

@Component({
  selector: 'app-type-single-header',
  templateUrl: './type-single-header.component.html',
  styleUrls: ['./type-single-header.component.scss']
})
export class TypeSingleHeaderComponent implements OnChanges {
  @Input() type!: any | null;
  @Input() isLoading!: boolean | null;

  typeName!: string;
  typeGeneration!: string;
  typeIcon!: string;
  typeClass!: string;
  typesClasses: any[] = ALL_TYPES;

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('type' in changes && this.type) {
      this.typeName = this.type?.name;
      this.typeGeneration = this.type?.generation?.name.replace('-', ' ');
      this.typeIcon = `assets/images/types/icons/${this.type?.name}.png`;
      this.typeClass = this.processType(this.type)[0].class;
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Returns the type's display information.
   *
   * @param       { string }      type
   * @returns     { string }
   */
  private processType(type: any): any {
    return this.typesClasses.filter((x) => x.class.toLowerCase().includes(type.name));
  }
}
