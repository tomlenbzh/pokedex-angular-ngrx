import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-type-single-card',
  templateUrl: './type-single-card.component.html',
  styleUrls: ['./type-single-card.component.scss']
})
export class TypeSingleCardComponent implements OnChanges {
  @Input() type!: any | null;
  @Input() isLoading!: boolean | null;

  info = {
    doubleDamageFrom: [] as any[],
    halfDamageFrom: [] as any[],
    doubleDamageTo: [] as any[],
    halfDamageTo: [] as any[]
  };

  constructor(private router: Router) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('type' in changes && this.type) this.initTypeCard();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Navigates to the type's details page.
   *
   * @param       { string }      type
   */
  navigateToType(type: string): void {
    this.router.navigateByUrl(`types/${type}`);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Initialiizes the type's basic information.
   */
  private initTypeCard(): void {
    this.info.doubleDamageFrom = this.type.damage_relations.double_damage_from.map((x: any) => ({
      name: x?.name,
      img: this.getTypeIcon(x?.name)
    }));
    this.info.halfDamageFrom = this.type.damage_relations.half_damage_from.map((x: any) => ({
      name: x?.name,
      img: this.getTypeIcon(x?.name)
    }));
    this.info.doubleDamageTo = this.type.damage_relations.double_damage_to.map((x: any) => ({
      name: x?.name,
      img: this.getTypeIcon(x?.name)
    }));
    this.info.halfDamageTo = this.type.damage_relations.half_damage_to.map((x: any) => ({
      name: x?.name,
      img: this.getTypeIcon(x?.name)
    }));
  }

  /**
   * Returns the proper icon regarding the specified type.
   *
   * @param       { string }      type
   * @returns     { string }
   */
  private getTypeIcon(type: string): string {
    return `assets/images/types/${type}.png`;
  }
}
