import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { TypeSingleCardComponent } from './type-single-card.component';

describe('TypeSingleCardComponent', () => {
  let component: TypeSingleCardComponent;
  let fixture: ComponentFixture<TypeSingleCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TypeSingleCardComponent],
      imports: [SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(TypeSingleCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
