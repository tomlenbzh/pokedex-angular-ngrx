import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-pokemons-type-single',
  templateUrl: './pokemons-type-single.component.html',
  styleUrls: ['./pokemons-type-single.component.scss']
})
export class PokemonsTypeSingleComponent {
  @Input() type!: any | null;
  @Input() isLoading!: boolean | null;
}
