import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SharedModule } from 'src/app/shared/shared.module';
import { PokemonsTypeSingleComponent } from './pokemons-type-single.component';
import { TypeSingleCardComponent } from './type-single-card/type-single-card.component';
import { TypeSingleHeaderComponent } from './type-single-header/type-single-header.component';

describe('PokemonsTypeSingleComponent', () => {
  let component: PokemonsTypeSingleComponent;
  let fixture: ComponentFixture<PokemonsTypeSingleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PokemonsTypeSingleComponent, TypeSingleCardComponent, TypeSingleHeaderComponent],
      imports: [SharedModule, LazyLoadImageModule]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonsTypeSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
