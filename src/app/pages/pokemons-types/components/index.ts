import { PokemonsTypeSingleComponent } from './pokemons-type-single/pokemons-type-single.component';
import { TypeSingleCardComponent } from './pokemons-type-single/type-single-card/type-single-card.component';
import { TypeSingleHeaderComponent } from './pokemons-type-single/type-single-header/type-single-header.component';
import { PokemonsTypesComponent } from './pokemons-types/pokemons-types.component';

export const components = [
  PokemonsTypesComponent,
  PokemonsTypeSingleComponent,
  TypeSingleHeaderComponent,
  TypeSingleCardComponent
];
