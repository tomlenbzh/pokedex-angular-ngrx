import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ALL_TYPES } from 'src/app/utils/constants/types.constants';

@Component({
  selector: 'app-pokemons-types',
  templateUrl: './pokemons-types.component.html',
  styleUrls: ['./pokemons-types.component.scss']
})
export class PokemonsTypesComponent {
  @Input() typesList: any[] | null = [];
  @Input() isLoading: boolean | null = false;

  typesClasses = ALL_TYPES;

  constructor(private router: Router) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  processType(type: any): any {
    return this.typesClasses.filter((x) => x.class.toLowerCase().includes(type.name));
  }

  /**
   * Returns the proper icon regarding the specified type.
   *
   * @param       { string }      type
   * @returns     { string }
   */
  getTypeIcon(type: string): string {
    return `assets/images/types/icons/${type}.png`;
  }

  /**
   * Navigates to the selected type details page.
   * @param       { string }      type
   */
  navigateToType(type: string): void {
    this.router.navigateByUrl(`types/${type}`);
  }
}
