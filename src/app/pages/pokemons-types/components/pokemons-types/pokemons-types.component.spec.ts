import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SharedModule } from 'src/app/shared/shared.module';
import { PokemonsTypesComponent } from './pokemons-types.component';

describe('PokemonsTypesComponent', () => {
  let component: PokemonsTypesComponent;
  let fixture: ComponentFixture<PokemonsTypesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PokemonsTypesComponent],
      imports: [SharedModule, LazyLoadImageModule]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonsTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
