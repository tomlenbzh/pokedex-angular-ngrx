import { PokemonsTypeSingleContainerComponent } from './pokemons-type-single-container/pokemons-type-single-container.component';
import { PokemonsTypesContainerComponent } from './pokemons-types-container/pokemons-types-container.component';

export const containers = [PokemonsTypesContainerComponent, PokemonsTypeSingleContainerComponent];
