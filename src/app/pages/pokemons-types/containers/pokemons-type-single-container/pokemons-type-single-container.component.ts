import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { TypeSingleHelper } from 'src/app/store/types/single/type-single.helper';

@Component({
  selector: 'app-pokemons-type-single-container',
  template: `<app-pokemons-type-single
    [type]="type | async"
    [isLoading]="isLoading | async"
  ></app-pokemons-type-single>`
})
export class PokemonsTypeSingleContainerComponent implements OnInit, OnDestroy {
  type!: Observable<any | null>;
  isLoading!: Observable<boolean | null>;

  private typeName!: string;
  private subscription!: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private typeSingleHelper: TypeSingleHelper) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.isLoading = this.typeSingleHelper.isLoading();
    this.type = this.typeSingleHelper.typeDetails();

    this.subscription = this.activatedRoute.params.subscribe((routeParams) => {
      this.typeName = routeParams['id'];
      this.typeSingleHelper.fetchTypeDetail(this.typeName);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
