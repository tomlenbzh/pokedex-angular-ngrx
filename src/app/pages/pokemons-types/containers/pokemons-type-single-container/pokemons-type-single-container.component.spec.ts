import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PokemonsTypeSingleContainerComponent } from './pokemons-type-single-container.component';
import * as fromRoot from '../../../../store';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from 'src/app/shared/shared.module';
import { PokemonsSingleContainerComponent } from 'src/app/pages/pokemons/containers/pokemons-single-container/pokemons-single-container.component';
import { PokemonsSingleComponent } from 'src/app/pages/pokemons/components/pokemons-single/pokemons-single.component';
import { PokemonsTypeSingleComponent } from '../../components/pokemons-type-single/pokemons-type-single.component';

describe('PokemonsTypeSingleContainerComponent', () => {
  let component: PokemonsTypeSingleContainerComponent;
  let fixture: ComponentFixture<PokemonsTypeSingleContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        PokemonsTypeSingleContainerComponent,
        PokemonsSingleContainerComponent,
        PokemonsSingleComponent,
        PokemonsTypeSingleComponent
      ],
      imports: [RouterTestingModule, StoreModule.forRoot({ ...fromRoot.reducers }), SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonsTypeSingleContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
