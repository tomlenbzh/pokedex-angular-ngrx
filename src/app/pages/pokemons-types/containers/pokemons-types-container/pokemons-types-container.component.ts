import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TypesListHelper } from 'src/app/store/types/list/types-list.helper';

@Component({
  selector: 'app-pokemons-types-container',
  template: '<app-pokemons-types [typesList]="typesList | async" [isLoading]="isLoading | async"></app-pokemons-types>'
})
export class PokemonsTypesContainerComponent implements OnInit {
  typesList!: Observable<any[] | null>;
  isLoading!: Observable<boolean | null>;

  constructor(private typesListHelper: TypesListHelper) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.isLoading = this.typesListHelper.isLoading();
    this.typesList = this.typesListHelper.typesList();

    this.typesListHelper.fetchTypesList();
  }
}
