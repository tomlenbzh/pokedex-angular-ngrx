import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PokemonsTypesComponent } from '../../components/pokemons-types/pokemons-types.component';
import { PokemonsTypesContainerComponent } from './pokemons-types-container.component';
import * as fromRoot from '../../../../store';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('PokemonsTypesContainerComponent', () => {
  let component: PokemonsTypesContainerComponent;
  let fixture: ComponentFixture<PokemonsTypesContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PokemonsTypesContainerComponent, PokemonsTypesComponent],
      imports: [RouterTestingModule, StoreModule.forRoot({ ...fromRoot.reducers }), SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonsTypesContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
