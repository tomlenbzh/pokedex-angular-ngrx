import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PokemonsTypesRoutingModule } from './pokemons-types-routing.module';
import { components } from './components';
import { containers } from './containers';
import { SharedModule } from 'src/app/shared/shared.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  declarations: [...components, ...containers],
  imports: [CommonModule, PokemonsTypesRoutingModule, SharedModule, LazyLoadImageModule]
})
export class PokemonsTypesModule {}
