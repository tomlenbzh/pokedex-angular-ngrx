import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EvolutionsHelper } from 'src/app/store/evolutions/evolutions.helper';
import { EvolutionsContainerComponent } from './evolutions-container.component';
import * as fromRoot from '../../../../store';
import { StoreModule } from '@ngrx/store';
import { EvolutionsComponent } from '../../components/evolutions/evolutions.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { PokemonLoaderComponent } from 'src/app/shared/components/pokemon-loader/pokemon-loader.component';
import { ListPaginatorComponent } from 'src/app/shared/components/list-paginator/list-paginator.component';
import { setMatIcons } from 'src/app/utils/methods/set-icons';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';

describe('EvolutionsContainerComponent', () => {
  let component: EvolutionsContainerComponent;
  let fixture: ComponentFixture<EvolutionsContainerComponent>;
  let domSanitizer: DomSanitizer;
  let matIconRegistry: MatIconRegistry;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EvolutionsContainerComponent, EvolutionsComponent, PokemonLoaderComponent, ListPaginatorComponent],
      imports: [StoreModule.forRoot({ ...fromRoot.reducers }), NgxPaginationModule, HttpClientModule, SharedModule],
      providers: [EvolutionsHelper, ScrollToService]
    }).compileComponents();

    fixture = TestBed.createComponent(EvolutionsContainerComponent);
    component = fixture.componentInstance;

    domSanitizer = TestBed.inject(DomSanitizer);
    matIconRegistry = TestBed.inject(MatIconRegistry);
    setMatIcons(matIconRegistry, domSanitizer);

    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
