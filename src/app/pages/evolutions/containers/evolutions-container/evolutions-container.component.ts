import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { EvolutionsHelper } from 'src/app/store/evolutions/evolutions.helper';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';

@Component({
  selector: 'app-evolutions-container',
  template: `<app-evolutions
    [evolutions]="evolutions | async"
    [meta]="meta | async"
    [isLoading]="isLoading | async"
    [itemsPerPage]="limit"
    [currentPage]="currentPage"
    (pageChanged)="onPageChange($event)"
  ></app-evolutions>`
})
export class EvolutionsContainerComponent implements OnInit {
  evolutions!: Observable<any[] | null>;
  meta!: Observable<IListMeta | null>;
  isLoading!: Observable<boolean | null>;

  limit = 20;
  offset = 0;

  constructor(private evolutionstHelper: EvolutionsHelper) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  get currentPage(): number {
    return Math.ceil((this.offset - 1) / this.limit) + 1;
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.meta = this.evolutionstHelper.meta();
    this.isLoading = this.evolutionstHelper.isLoading();
    this.evolutions = this.evolutionstHelper.evolutions();

    this.fetchEvolutions();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Sets the new offset regarding the selected page anf retrieves a new list of Pokémons.
   *
   * @param     { number }      page
   */
  onPageChange(page: number): void {
    const newOffset: number = (page - 1) * this.limit;
    this.offset = newOffset;
    this.fetchEvolutions();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Retrieves a list of Pokémons' evolutions
   */
  private fetchEvolutions(): void {
    this.evolutionstHelper.fetchEvolutions(this.limit, this.offset);
  }
}
