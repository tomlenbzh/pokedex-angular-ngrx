import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { EvolutionNodeComponent } from './evolution-node.component';

describe('EvolutionNodeComponent', () => {
  let component: EvolutionNodeComponent;
  let fixture: ComponentFixture<EvolutionNodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EvolutionNodeComponent],
      imports: [LazyLoadImageModule]
    }).compileComponents();

    fixture = TestBed.createComponent(EvolutionNodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
