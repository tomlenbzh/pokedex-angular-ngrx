import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-evolution-node',
  templateUrl: './evolution-node.component.html',
  styleUrls: ['./evolution-node.component.scss']
})
export class EvolutionNodeComponent implements OnChanges {
  @Input() pokemon: any;

  hasGender: boolean = false;
  gender: string = '';
  pokemonName!: string;
  pokemonId!: string;
  evolItem!: string;
  placeholder = 'assets/images/img-placeholder.gif';

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  get isFirstPokemon(): boolean {
    return this.pokemon?.minLevel === 1 && this.pokemon?.triggerName === null ? true : false;
  }

  constructor(private router: Router) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('pokemon' in changes && this.pokemon) this.initEvolution();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Navigates to the selected pokemon details' page
   *
   * @param     { number }      pokemonId
   */
  navigateToPokemon(pokemonId: number): void {
    this.router.navigateByUrl(`/pokemons/${pokemonId}`);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Sets the basic node Pokémon information.
   */
  private initEvolution(): void {
    this.pokemonId = this.processId(this.pokemon?.speciesId);
    this.pokemonName = this.checkGender(this.pokemon?.speciesName);
    this.evolItem = this.pokemon?.item?.name?.replace('-', ' ');
  }

  /**
   * Retrieves the proper icon if the Pokémon's gender is specified
   *
   * @param     { string }      name
   * @returns   { string }
   */
  private checkGender(name: string): string {
    const splitName = name.split('-');

    if (splitName.length > 1) {
      this.hasGender = true;
      this.gender = splitName[1] === 'f' ? 'female' : 'male';
    }

    return name.split('-')[0];
  }

  /**
   * Returns the styles Pokémon id.
   *
   * @param       { number }      pokemonId
   * @returns     { string }
   */
  private processId(pokemonId: number): string {
    const paddedId = `${pokemonId}`.padStart(3, '0');
    return `#${paddedId}`;
  }
}
