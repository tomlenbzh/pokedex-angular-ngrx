import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxPaginationModule } from 'ngx-pagination';
import { ListPaginatorComponent } from 'src/app/shared/components/list-paginator/list-paginator.component';
import { PokemonLoaderComponent } from 'src/app/shared/components/pokemon-loader/pokemon-loader.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { setMatIcons } from 'src/app/utils/methods/set-icons';
import { EvolutionNodeComponent } from '../evolution-node/evolution-node.component';
import { EvolutionTreeComponent } from '../evolution-tree/evolution-tree.component';
import { EvolutionsComponent } from './evolutions.component';
import { HttpClientModule } from '@angular/common/http';
import { ScrollToModule, ScrollToService } from '@nicky-lenaers/ngx-scroll-to';

describe('EvolutionsComponent', () => {
  let component: EvolutionsComponent;
  let fixture: ComponentFixture<EvolutionsComponent>;

  let domSanitizer: DomSanitizer;
  let matIconRegistry: MatIconRegistry;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        EvolutionsComponent,
        PokemonLoaderComponent,
        EvolutionNodeComponent,
        EvolutionTreeComponent,
        ListPaginatorComponent
      ],
      imports: [NgxPaginationModule, SharedModule, HttpClientModule, ScrollToModule],
      providers: [ScrollToService]
    }).compileComponents();

    fixture = TestBed.createComponent(EvolutionsComponent);
    component = fixture.componentInstance;

    domSanitizer = TestBed.inject(DomSanitizer);
    matIconRegistry = TestBed.inject(MatIconRegistry);
    setMatIcons(matIconRegistry, domSanitizer);

    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
