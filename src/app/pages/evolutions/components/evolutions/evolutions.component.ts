import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';

@Component({
  selector: 'app-evolutions',
  templateUrl: './evolutions.component.html',
  styleUrls: ['./evolutions.component.scss']
})
export class EvolutionsComponent implements OnChanges {
  @Input() evolutions: any[] | null = [];
  @Input() isLoading: boolean | null = false;
  @Input() meta!: IListMeta | null;

  @Input() itemsPerPage!: number;
  @Input() currentPage!: number;
  @Input() totalItems!: number;

  @Output() pageChanged: EventEmitter<number> = new EventEmitter<number>();

  loadingMessage = 'Evolutions loading...';
  displayedEvolutions: any[] | null = [];

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('evolutions' in changes && this.evolutions) {
      this.displayedEvolutions = [...this.evolutions];
      this.displayedEvolutions?.sort((a: any, b: any) => a[0]?.speciesId - b[0]?.speciesId) || [];
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Emits the selected page number to the parent component.
   *
   * @param     { any }      page
   */
  onPageChange(page: any): void {
    this.pageChanged.emit(page as number);
  }
}
