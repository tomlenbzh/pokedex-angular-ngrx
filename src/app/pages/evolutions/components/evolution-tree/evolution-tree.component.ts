import { Component, Input } from '@angular/core';
import { RotateIcon, TogglePanel } from 'src/app/shared/animations/animations';

@Component({
  selector: 'app-evolution-tree',
  templateUrl: './evolution-tree.component.html',
  styleUrls: ['./evolution-tree.component.scss'],
  animations: [TogglePanel, RotateIcon]
})
export class EvolutionTreeComponent {
  @Input() evolutionTree: any;

  showEvolutions: boolean = false;

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  get hasEvolutions(): boolean {
    return this.evolutionTree?.length > 1;
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Toggles whether or not the Pokémon's evolutions should be displayed.
   */
  toggleEvolutionTree(): void {
    this.showEvolutions = !this.showEvolutions;
  }

  /**
   * Returns whether or not the current Pokémon is the first in the evolutions tree.
   *
   * @param     { any }       pokemon
   * @returns   { boolean }
   */
  isFirstPokemon(pokemon: any): boolean {
    return pokemon.minLevel === 1 && pokemon.triggerName === null;
  }
}
