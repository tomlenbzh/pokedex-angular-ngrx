import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { EvolutionNodeComponent } from '../evolution-node/evolution-node.component';
import { EvolutionTreeComponent } from './evolution-tree.component';

describe('EvolutionTreeComponent', () => {
  let component: EvolutionTreeComponent;
  let fixture: ComponentFixture<EvolutionTreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EvolutionTreeComponent, EvolutionNodeComponent],
      imports: [SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(EvolutionTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
