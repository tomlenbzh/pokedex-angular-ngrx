import { EvolutionNodeComponent } from './evolution-node/evolution-node.component';
import { EvolutionTreeComponent } from './evolution-tree/evolution-tree.component';
import { EvolutionsComponent } from './evolutions/evolutions.component';

export const components = [EvolutionsComponent, EvolutionNodeComponent, EvolutionTreeComponent];
