import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvolutionsRoutingModule } from './evolutions-routing.module';
import { container } from './containers';
import { components } from './components';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  declarations: [...container, ...components],
  imports: [CommonModule, EvolutionsRoutingModule, SharedModule, NgxPaginationModule, LazyLoadImageModule]
})
export class EvolutionsModule {}
