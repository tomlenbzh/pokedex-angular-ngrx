import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EvolutionsContainerComponent } from './containers/evolutions-container/evolutions-container.component';

const routes: Routes = [
  { path: '', component: EvolutionsContainerComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvolutionsRoutingModule {}
