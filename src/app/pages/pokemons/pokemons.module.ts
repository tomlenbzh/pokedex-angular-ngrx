import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxPaginationModule } from 'ngx-pagination';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SharedModule } from 'src/app/shared/shared.module';

import { PokemonsRoutingModule } from './pokemons-routing.module';
import { containers } from './containers';
import { components } from './components';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [...containers, ...components],
  imports: [
    CommonModule,
    PokemonsRoutingModule,
    NgxPaginationModule,
    LazyLoadImageModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PokemonsModule {}
