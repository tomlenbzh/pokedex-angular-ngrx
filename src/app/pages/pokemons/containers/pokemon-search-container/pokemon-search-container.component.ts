import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PokemonsSearchHelper } from 'src/app/store/pokemons/search/pokemons-search.helper';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';

@Component({
  selector: 'app-pokemon-search-container',
  template: `<app-pokemon-search [pokemonsSearch]="pokemonsSearch | async"></app-pokemon-search>`
})
export class PokemonSearchContainerComponent implements OnInit {
  pokemonsSearch!: Observable<any[] | null>;
  meta!: Observable<IListMeta | null>;
  isLoading!: Observable<boolean | null>;

  constructor(private pokemonsSearchHelper: PokemonsSearchHelper) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.meta = this.pokemonsSearchHelper.meta();
    this.isLoading = this.pokemonsSearchHelper.isLoading();
    this.pokemonsSearch = this.pokemonsSearchHelper.pokemonsSearch();
  }
}
