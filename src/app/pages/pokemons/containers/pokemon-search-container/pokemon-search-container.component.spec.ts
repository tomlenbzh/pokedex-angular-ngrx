import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PokemonsSearchHelper } from 'src/app/store/pokemons/search/pokemons-search.helper';
import { PokemonSearchContainerComponent } from './pokemon-search-container.component';
import * as fromRoot from '../../../../store';
import { StoreModule } from '@ngrx/store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PokemonSearchComponent } from '../../components/pokemon-search/pokemon-search.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { setMatIcons } from 'src/app/utils/methods/set-icons';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

describe('PokemonSearchContainerComponent', () => {
  let component: PokemonSearchContainerComponent;
  let fixture: ComponentFixture<PokemonSearchContainerComponent>;
  let domSanitizer: DomSanitizer;
  let matIconRegistry: MatIconRegistry;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PokemonSearchContainerComponent, PokemonSearchComponent],
      imports: [
        SharedModule,
        StoreModule.forRoot({ ...fromRoot.reducers }),
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientModule
      ],
      providers: [PokemonsSearchHelper]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonSearchContainerComponent);
    component = fixture.componentInstance;

    domSanitizer = TestBed.inject(DomSanitizer);
    matIconRegistry = TestBed.inject(MatIconRegistry);
    setMatIcons(matIconRegistry, domSanitizer);

    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
