import { PokemonSearchContainerComponent } from './pokemon-search-container/pokemon-search-container.component';
import { PokemonsListContainerComponent } from './pokemons-list-container/pokemons-list-container.component';
import { PokemonsSingleContainerComponent } from './pokemons-single-container/pokemons-single-container.component';

export const containers = [
  PokemonsListContainerComponent,
  PokemonsSingleContainerComponent,
  PokemonSearchContainerComponent
];
