import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PokemonsListContainerComponent } from './pokemons-list-container.component';
import * as fromRoot from '../../../../store';
import { StoreModule } from '@ngrx/store';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { PokemonsListComponent } from '../../components/pokemons-list/pokemons-list.component';
import { setMatIcons } from 'src/app/utils/methods/set-icons';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';
import { PokemonSearchContainerComponent } from '../pokemon-search-container/pokemon-search-container.component';
import { PokemonSearchComponent } from '../../components/pokemon-search/pokemon-search.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('PokemonsListContainerComponent', () => {
  let component: PokemonsListContainerComponent;
  let fixture: ComponentFixture<PokemonsListContainerComponent>;
  let domSanitizer: DomSanitizer;
  let matIconRegistry: MatIconRegistry;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        PokemonsListContainerComponent,
        PokemonSearchContainerComponent,
        PokemonSearchComponent,
        PokemonsListComponent
      ],
      imports: [
        StoreModule.forRoot({ ...fromRoot.reducers }),
        NgxPaginationModule,
        HttpClientModule,
        SharedModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [ScrollToService]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonsListContainerComponent);
    component = fixture.componentInstance;

    domSanitizer = TestBed.inject(DomSanitizer);
    matIconRegistry = TestBed.inject(MatIconRegistry);
    setMatIcons(matIconRegistry, domSanitizer);

    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
