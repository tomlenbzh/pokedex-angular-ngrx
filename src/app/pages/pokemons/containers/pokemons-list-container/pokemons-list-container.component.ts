import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PokemonsListHelper } from 'src/app/store/pokemons/list/pokemons-list.helper';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';

@Component({
  selector: 'app-pokemons-list-container',
  template: ` <app-pokemons-list
    [pokemonsList]="pokemonsList | async"
    [meta]="meta | async"
    [isLoading]="isLoading | async"
    [itemsPerPage]="limit"
    [currentPage]="currentPage"
    (pageChanged)="onPageChange($event)"
  ></app-pokemons-list>`
})
export class PokemonsListContainerComponent implements OnInit {
  pokemonsList!: Observable<any[] | null>;
  meta!: Observable<IListMeta | null>;
  isLoading!: Observable<boolean | null>;

  limit = 20;
  private offset = 0;

  constructor(private pokemonsListHelper: PokemonsListHelper) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  get currentPage(): number {
    return Math.ceil((this.offset - 1) / this.limit) + 1;
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.meta = this.pokemonsListHelper.meta();
    this.isLoading = this.pokemonsListHelper.isLoading();
    this.pokemonsList = this.pokemonsListHelper.pokemonsList();

    this.fetchPokemons();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Sets the new offset regarding the selected page anf retrieves a new list of Pokémons.
   *
   * @param     { number }      page
   */
  onPageChange(page: number): void {
    const newOffset: number = (page - 1) * this.limit;
    this.offset = newOffset;
    this.fetchPokemons();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Retrieves a list of Pokémons
   */
  private fetchPokemons(): void {
    this.pokemonsListHelper.fetchPokemonsList(this.limit, this.offset);
  }
}
