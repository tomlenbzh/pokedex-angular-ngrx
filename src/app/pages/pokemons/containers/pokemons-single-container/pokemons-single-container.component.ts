import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { PokemonsSingleHelper } from 'src/app/store/pokemons/single/pokemons-single.helper';

@Component({
  selector: 'app-pokemons-single-container',
  template: '<app-pokemons-single [pokemon]="pokemon | async" [isLoading]="isLoading | async"></app-pokemons-single>'
})
export class PokemonsSingleContainerComponent implements OnInit {
  pokemon!: Observable<any | null>;
  isLoading!: Observable<boolean | null>;

  pokemonId!: number;

  constructor(private activatedRoute: ActivatedRoute, private pokemonsSingleHelper: PokemonsSingleHelper) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.pokemonId = this.activatedRoute.snapshot.params['id'];
    this.isLoading = this.pokemonsSingleHelper.isLoading();
    this.pokemon = this.pokemonsSingleHelper.pokemon();

    this.pokemonId && this.pokemonsSingleHelper.fetchPokemonDetail(this.pokemonId);
  }
}
