import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PokemonsSingleComponent } from '../../components/pokemons-single/pokemons-single.component';
import { PokemonsSingleContainerComponent } from './pokemons-single-container.component';
import * as fromRoot from '../../../../store';
import { StoreModule } from '@ngrx/store';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { setMatIcons } from 'src/app/utils/methods/set-icons';
import { RouterTestingModule } from '@angular/router/testing';
import { PokemonMovesListComponent } from '../../components/pokemons-single/pokemon-moves-list/pokemon-moves-list.component';
import { PokemonAbilitiesListComponent } from '../../components/pokemons-single/pokemon-abilities-list/pokemon-abilities-list.component';
import { PokemonSpritesGalleryComponent } from '../../components/pokemons-single/pokemon-sprites-gallery/pokemon-sprites-gallery.component';
import { PokemonStatsListComponent } from '../../components/pokemons-single/pokemon-stats-list/pokemon-stats-list.component';
import { PokemonDetailHeaderComponent } from '../../components/pokemons-single/pokemon-detail-header/pokemon-detail-header.component';
import { LazyLoadImageModule } from 'ng-lazyload-image';

describe('PokemonsSingleContainerComponent', () => {
  let component: PokemonsSingleContainerComponent;
  let fixture: ComponentFixture<PokemonsSingleContainerComponent>;
  let domSanitizer: DomSanitizer;
  let matIconRegistry: MatIconRegistry;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        PokemonsSingleContainerComponent,
        PokemonsSingleComponent,
        PokemonMovesListComponent,
        PokemonAbilitiesListComponent,
        PokemonSpritesGalleryComponent,
        PokemonStatsListComponent,
        PokemonDetailHeaderComponent
      ],
      imports: [
        StoreModule.forRoot({ ...fromRoot.reducers }),
        NgxPaginationModule,
        HttpClientModule,
        SharedModule,
        RouterTestingModule,
        LazyLoadImageModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonsSingleContainerComponent);
    component = fixture.componentInstance;

    domSanitizer = TestBed.inject(DomSanitizer);
    matIconRegistry = TestBed.inject(MatIconRegistry);
    setMatIcons(matIconRegistry, domSanitizer);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
