import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PokemonsListContainerComponent } from './containers/pokemons-list-container/pokemons-list-container.component';
import { PokemonsSingleContainerComponent } from './containers/pokemons-single-container/pokemons-single-container.component';

const routes: Routes = [
  { path: '', component: PokemonsListContainerComponent },
  { path: ':id', component: PokemonsSingleContainerComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PokemonsRoutingModule {}
