import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SharedModule } from 'src/app/shared/shared.module';
import { PokemonSpritesGalleryComponent } from './pokemon-sprites-gallery.component';

describe('PokemonSpritesGalleryComponent', () => {
  let component: PokemonSpritesGalleryComponent;
  let fixture: ComponentFixture<PokemonSpritesGalleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PokemonSpritesGalleryComponent],
      imports: [LazyLoadImageModule, SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonSpritesGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
