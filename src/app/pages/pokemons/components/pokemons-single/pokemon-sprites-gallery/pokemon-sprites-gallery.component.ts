import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-pokemon-sprites-gallery',
  templateUrl: './pokemon-sprites-gallery.component.html',
  styleUrls: ['./pokemon-sprites-gallery.component.scss']
})
export class PokemonSpritesGalleryComponent {
  @Input() spritesList: any;
}
