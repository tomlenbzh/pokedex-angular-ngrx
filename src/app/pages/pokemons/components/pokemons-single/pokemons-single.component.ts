import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-pokemons-single',
  templateUrl: './pokemons-single.component.html',
  styleUrls: ['./pokemons-single.component.scss']
})
export class PokemonsSingleComponent {
  @Input() pokemon: any;
  @Input() isLoading: boolean | null = false;

  loadingMessage = 'Loading Pokémon details...';
}
