import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { PokemonLoaderComponent } from 'src/app/shared/components/pokemon-loader/pokemon-loader.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PokemonAbilitiesListComponent } from './pokemon-abilities-list/pokemon-abilities-list.component';
import { PokemonDetailHeaderComponent } from './pokemon-detail-header/pokemon-detail-header.component';
import { PokemonMovesListComponent } from './pokemon-moves-list/pokemon-moves-list.component';
import { PokemonSpritesGalleryComponent } from './pokemon-sprites-gallery/pokemon-sprites-gallery.component';
import { PokemonStatsListComponent } from './pokemon-stats-list/pokemon-stats-list.component';
import { PokemonsSingleComponent } from './pokemons-single.component';

describe('PokemonsSingleComponent', () => {
  let component: PokemonsSingleComponent;
  let fixture: ComponentFixture<PokemonsSingleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        PokemonsSingleComponent,
        PokemonLoaderComponent,
        PokemonDetailHeaderComponent,
        PokemonStatsListComponent,
        PokemonMovesListComponent,
        PokemonSpritesGalleryComponent,
        PokemonAbilitiesListComponent
      ],
      imports: [LazyLoadImageModule, SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonsSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
