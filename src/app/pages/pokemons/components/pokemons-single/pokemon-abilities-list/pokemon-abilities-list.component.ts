import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-pokemon-abilities-list',
  templateUrl: './pokemon-abilities-list.component.html',
  styleUrls: ['./pokemon-abilities-list.component.scss']
})
export class PokemonAbilitiesListComponent {
  @Input() abilitiesList: any;
}
