import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { PokemonAbilitiesListComponent } from './pokemon-abilities-list.component';

describe('PokemonAbilitiesListComponent', () => {
  let component: PokemonAbilitiesListComponent;
  let fixture: ComponentFixture<PokemonAbilitiesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PokemonAbilitiesListComponent],
      imports: [SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonAbilitiesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
