import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SharedModule } from 'src/app/shared/shared.module';
import { PokemonDetailHeaderComponent } from './pokemon-detail-header.component';

describe('PokemonDetailHeaderComponent', () => {
  let component: PokemonDetailHeaderComponent;
  let fixture: ComponentFixture<PokemonDetailHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PokemonDetailHeaderComponent],
      imports: [LazyLoadImageModule, SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonDetailHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
