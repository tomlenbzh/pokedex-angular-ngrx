import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { TypesGif, ALL_TYPES } from 'src/app/utils/constants/types.constants';

@Component({
  selector: 'app-pokemon-detail-header',
  templateUrl: './pokemon-detail-header.component.html',
  styleUrls: ['./pokemon-detail-header.component.scss']
})
export class PokemonDetailHeaderComponent implements OnChanges {
  @Input() pokemon!: any;

  pokemontypes: any[] = [];
  typesList = ALL_TYPES;
  hasGif!: boolean;
  pokemonGif!: string;
  selectedGifClass = { type: '', class: '' };

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  get height(): number {
    return this.pokemon?.height / 10;
  }

  get weight(): number {
    return this.pokemon?.weight / 10;
  }

  constructor(private router: Router) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('pokemon' in changes && this.pokemon) this.initPokemonInfo();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Sets the basic Pokémon displayed information.
   */
  initPokemonInfo(): void {
    this.pokemontypes = this.pokemon?.types.map((x: any) => ({
      name: x?.type?.name,
      img: `assets/images/types/${x?.type?.name}.png`
    }));

    this.hasGif = this.checkGif(this.pokemon?.color[0]?.label);
    this.hasGif === true && (this.pokemonGif = this.getGif(this.pokemon?.color[0]?.label));
  }

  /**
   * Navigates to the selected route.
   *
   * @param     { string }      type
   */
  navigateToTypeDetails(type: any): void {
    this.router.navigateByUrl(`types/${type}`);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Returns whether or not a .GIF file matches the current type.
   *
   * @param     { string }      type
   * @returns   { boolean }
   */
  private checkGif(type: string): boolean {
    const supportedTypes = TypesGif;
    this.selectedGifClass.class = `${type.toLowerCase()}-gif`;
    return supportedTypes.includes(type) ? true : false;
  }

  /**
   * Returns the path towards the proper .GIF file
   *
   * @param     { string }      type
   * @returns   { string }
   */
  private getGif(type: string): string {
    this.selectedGifClass.type = type;
    return `assets/images/gifs/${type.toLowerCase()}.gif`;
  }
}
