import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { PokemonStatsListComponent } from './pokemon-stats-list.component';

describe('PokemonStatsListComponent', () => {
  let component: PokemonStatsListComponent;
  let fixture: ComponentFixture<PokemonStatsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PokemonStatsListComponent],
      imports: [SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonStatsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
