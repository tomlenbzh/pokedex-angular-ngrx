import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-pokemon-stats-list',
  templateUrl: './pokemon-stats-list.component.html',
  styleUrls: ['./pokemon-stats-list.component.scss']
})
export class PokemonStatsListComponent {
  @Input() statsList: any;
}
