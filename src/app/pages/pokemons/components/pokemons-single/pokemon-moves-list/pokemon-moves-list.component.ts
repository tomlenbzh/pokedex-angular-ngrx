import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-moves-list',
  templateUrl: './pokemon-moves-list.component.html',
  styleUrls: ['./pokemon-moves-list.component.scss']
})
export class PokemonMovesListComponent implements OnChanges {
  @Input() movesList: any;

  isLoading = true;
  editedMovesList: any[] = [];

  constructor(private router: Router) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('movesList' in changes && this.movesList) this.getTypesIcons();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Navigates to the selected route.
   *
   * @param     { string }      type
   */
  navigateToTypeDetails(type: string): void {
    this.router.navigateByUrl(`types/${type}`);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Retrives the proper icons regarding the Pokémon's types.
   */
  private getTypesIcons(): void {
    const path = 'assets/images/types';

    this.editedMovesList = this.movesList
      .map((data: any) => ({ data, img: `${path}/${data.detail.type.name}.png` }))
      .sort((a: any, b: any) => a.data.detail.type.name.localeCompare(b.data.detail.type.name));

    this.isLoading = false;
  }
}
