import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SharedModule } from 'src/app/shared/shared.module';
import { PokemonMovesListComponent } from './pokemon-moves-list.component';

describe('PokemonMovesListComponent', () => {
  let component: PokemonMovesListComponent;
  let fixture: ComponentFixture<PokemonMovesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PokemonMovesListComponent],
      imports: [LazyLoadImageModule, SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonMovesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
