import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';

@Component({
  selector: 'app-pokemons-list',
  templateUrl: './pokemons-list.component.html',
  styleUrls: ['./pokemons-list.component.scss']
})
export class PokemonsListComponent {
  @Input() pokemonsList: any[] | null = [];
  @Input() meta!: IListMeta | null;
  @Input() isLoading: boolean | null = false;

  @Input() itemsPerPage!: number;
  @Input() currentPage!: number;
  @Input() totalItems!: number;

  @Input() maxSize = 6;
  @Input() disabled = false;

  @Output() pageChanged: EventEmitter<number> = new EventEmitter<number>();

  loadingMessage = 'Loading Pokémons...';

  constructor(private router: Router) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Emits the selected page number to the parent component.
   *
   * @param     { any }      page
   */
  onPageChange(page: any): void {
    this.pageChanged.emit(page as number);
  }

  /**
   * Navigates to the selected pokemon details' page
   *
   * @param     { number }      pokemonId
   */
  navigateToPokemon(pokemonId: number): void {
    this.router.navigateByUrl(`/pokemons/${pokemonId}`);
  }
}
