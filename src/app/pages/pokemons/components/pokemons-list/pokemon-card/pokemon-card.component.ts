import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss']
})
export class PokemonCardComponent implements OnChanges {
  @Input() pokemon!: any;

  pokemonTypes: any[] = [];
  hasGender: boolean = false;
  gender = '';
  customClasses = '';
  errorPlaceholder = 'assets/images/img-placeholder.gif';

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('pokemon' in changes && this.pokemon) {
      this.checkGender(this.pokemon.name);
      this.getPokemonTypes();
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Retrieves the proper icon if the Pokémon's gender is specified
   *
   * @param     { string }      name
   * @returns   { string }
   */
  private checkGender(name: string): string {
    const splitName = name.split('-');

    if (splitName.length > 1) {
      this.hasGender = true;
      this.gender = splitName[1] === 'f' ? 'female' : 'male';
    }

    return name.split('-')[0];
  }

  /**
   * Retrieves the correct image and labels for Pokémon types.
   */
  private getPokemonTypes(): void {
    this.customClasses = this.pokemon.typesInfo[0]?.class;
    this.pokemonTypes = this.pokemon?.types.map((slot: any) => ({
      name: slot.type.name,
      img: `assets/images/types/${slot?.type?.name}.png`
    }));
  }
}
