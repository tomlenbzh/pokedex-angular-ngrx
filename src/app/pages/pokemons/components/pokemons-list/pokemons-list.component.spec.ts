import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from 'src/app/shared/shared.module';
import { PokemonCardComponent } from './pokemon-card/pokemon-card.component';
import { PokemonsListComponent } from './pokemons-list.component';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { setMatIcons } from 'src/app/utils/methods/set-icons';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';
import { PokemonSearchContainerComponent } from '../../containers/pokemon-search-container/pokemon-search-container.component';
import * as fromRoot from '../../../../store';
import { StoreModule } from '@ngrx/store';
import { PokemonSearchComponent } from '../pokemon-search/pokemon-search.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('PokemonsListComponent', () => {
  let component: PokemonsListComponent;
  let fixture: ComponentFixture<PokemonsListComponent>;
  let domSanitizer: DomSanitizer;
  let matIconRegistry: MatIconRegistry;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        PokemonsListComponent,
        PokemonCardComponent,
        PokemonSearchContainerComponent,
        PokemonSearchComponent
      ],
      imports: [
        NgxPaginationModule,
        SharedModule,
        BrowserAnimationsModule,
        HttpClientModule,
        StoreModule.forRoot({ ...fromRoot.reducers }),
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [ScrollToService]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonsListComponent);
    component = fixture.componentInstance;

    domSanitizer = TestBed.inject(DomSanitizer);
    matIconRegistry = TestBed.inject(MatIconRegistry);
    setMatIcons(matIconRegistry, domSanitizer);

    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
