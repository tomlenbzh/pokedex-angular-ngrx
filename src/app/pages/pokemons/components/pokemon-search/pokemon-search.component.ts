import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { map, Observable, startWith } from 'rxjs';

@Component({
  selector: 'app-pokemon-search',
  templateUrl: './pokemon-search.component.html',
  styleUrls: ['./pokemon-search.component.scss']
})
export class PokemonSearchComponent implements OnChanges {
  @Input() pokemonsSearch: any[] | null = [];

  searchControl = new FormControl('');
  filteredOptions!: Observable<any[]>;
  placeholder = 'assets/images/Pokeball-Loader.png';

  constructor(private router: Router) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('pokemonsSearch' in changes) {
      this.filteredOptions = this.searchControl.valueChanges.pipe(
        startWith(''),
        map((value) => this.filter(value || ''))
      );
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Navigates to the selected Pokémon's details.
   *
   * @param       { any }      selected
   */
  navigateToPokemonDetail(selected: any): void {
    const pokemon: number = selected.option.value;
    this.router.navigateByUrl(`pokemons/${pokemon}`);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Filters the Pokémons list by their names.
   *
   * @param       { string }      value
   * @returns     { string[] }
   */
  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.pokemonsSearch!.filter((pokemon) => pokemon?.name.toLowerCase().includes(filterValue));
  }
}
