import { PokemonSearchComponent } from './pokemon-search/pokemon-search.component';
import { PokemonCardComponent } from './pokemons-list/pokemon-card/pokemon-card.component';
import { PokemonsListComponent } from './pokemons-list/pokemons-list.component';
import { PokemonAbilitiesListComponent } from './pokemons-single/pokemon-abilities-list/pokemon-abilities-list.component';
import { PokemonDetailHeaderComponent } from './pokemons-single/pokemon-detail-header/pokemon-detail-header.component';
import { PokemonMovesListComponent } from './pokemons-single/pokemon-moves-list/pokemon-moves-list.component';
import { PokemonSpritesGalleryComponent } from './pokemons-single/pokemon-sprites-gallery/pokemon-sprites-gallery.component';
import { PokemonStatsListComponent } from './pokemons-single/pokemon-stats-list/pokemon-stats-list.component';
import { PokemonsSingleComponent } from './pokemons-single/pokemons-single.component';

export const components = [
  PokemonsListComponent,
  PokemonsSingleComponent,
  PokemonCardComponent,
  PokemonAbilitiesListComponent,
  PokemonMovesListComponent,
  PokemonDetailHeaderComponent,
  PokemonSpritesGalleryComponent,
  PokemonStatsListComponent,
  PokemonSearchComponent
];
