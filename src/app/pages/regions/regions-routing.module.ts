import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegionsContainerComponent } from './containers/regions-container/regions-container.component';

const routes: Routes = [
  { path: '', component: RegionsContainerComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegionsRoutingModule {}
