import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RegionsContainerComponent } from './regions-container.component';
import * as fromRoot from '../../../../store';
import { StoreModule } from '@ngrx/store';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { setMatIcons } from 'src/app/utils/methods/set-icons';
import { RegionsComponent } from '../../components/regions/regions.component';

describe('RegionsContainerComponent', () => {
  let component: RegionsContainerComponent;
  let fixture: ComponentFixture<RegionsContainerComponent>;
  let domSanitizer: DomSanitizer;
  let matIconRegistry: MatIconRegistry;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegionsContainerComponent, RegionsComponent],
      imports: [StoreModule.forRoot({ ...fromRoot.reducers }), NgxPaginationModule, HttpClientModule, SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(RegionsContainerComponent);
    component = fixture.componentInstance;

    domSanitizer = TestBed.inject(DomSanitizer);
    matIconRegistry = TestBed.inject(MatIconRegistry);
    setMatIcons(matIconRegistry, domSanitizer);

    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
