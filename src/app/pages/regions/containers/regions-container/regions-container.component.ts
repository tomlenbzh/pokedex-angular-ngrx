import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RegionsListHelper } from 'src/app/store/regions/regions.herlper';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';

@Component({
  selector: 'app-regions-container',
  template: ` <app-regions
    [regionsList]="regionsList | async"
    [meta]="meta | async"
    [isLoading]="isLoading | async"
  ></app-regions>`
})
export class RegionsContainerComponent implements OnInit {
  regionsList!: Observable<any[] | null>;
  meta!: Observable<IListMeta | null>;
  isLoading!: Observable<boolean | null>;

  constructor(private regionsListHelper: RegionsListHelper) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.meta = this.regionsListHelper.meta();
    this.isLoading = this.regionsListHelper.isLoading();
    this.regionsList = this.regionsListHelper.regionsList();

    this.regionsListHelper.fetchPokemonsList();
  }
}
