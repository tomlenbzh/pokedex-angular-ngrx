import { RegionCardComponent } from './regions/region-card/region-card.component';
import { RegionsComponent } from './regions/regions.component';

export const components = [RegionsComponent, RegionCardComponent];
