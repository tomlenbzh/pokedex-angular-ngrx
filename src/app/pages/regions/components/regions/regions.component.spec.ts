import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgxPaginationModule } from 'ngx-pagination';
import { PokemonLoaderComponent } from 'src/app/shared/components/pokemon-loader/pokemon-loader.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RegionCardComponent } from './region-card/region-card.component';
import { RegionsComponent } from './regions.component';

describe('RegionsComponent', () => {
  let component: RegionsComponent;
  let fixture: ComponentFixture<RegionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegionsComponent, RegionCardComponent, PokemonLoaderComponent],
      imports: [SharedModule, NgxPaginationModule]
    }).compileComponents();

    fixture = TestBed.createComponent(RegionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
