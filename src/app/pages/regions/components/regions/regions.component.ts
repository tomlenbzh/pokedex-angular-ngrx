import { Component, Input } from '@angular/core';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';

@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.scss']
})
export class RegionsComponent {
  @Input() regionsList: any[] | null = [];
  @Input() meta!: IListMeta | null;
  @Input() isLoading: boolean | null = false;

  loadingMessage = 'Loading regions details...';
}
