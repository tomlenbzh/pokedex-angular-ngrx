import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { RotateIcon, ToggleRegion } from 'src/app/utils/animations/regions.animation';

@Component({
  selector: 'app-region-card',
  templateUrl: './region-card.component.html',
  styleUrls: ['./region-card.component.scss'],
  animations: [ToggleRegion, RotateIcon]
})
export class RegionCardComponent implements OnChanges {
  @Input() region: any;

  showRegionInfo: boolean = false;
  regionGeneration!: string;
  regionLocationsNumber!: string;
  regionMap!: string;
  versionsNamesList: any[] = [];
  versionList: any[] = [];
  errorPlaceholder = 'assets/images/img-placeholder.gif';

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('region' in changes && this.region) {
      this.regionGeneration = this.region?.main_generation?.name?.replace('-', ' ');
      this.regionLocationsNumber = this.region?.locations?.length;
      this.processVersions();

      this.versionList = this.versionsNamesList.map((version: any) => {
        return {
          name: this.processVersionsNames(version),
          capitalName: version.toUpperCase().replace('-', ' '),
          cover: `assets/images/versions/${version}.png`
        };
      });
      this.regionMap = `../../../assets/images/regions/${this.region?.name}.png`;
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Stores re-written games versions names.
   */
  processVersions(): any {
    this.versionsNamesList = this.region.version_groups
      .map((x: any) => this.processVersionsNames(x.name))
      .reduce((acc: any, val: any) => acc.concat(val), []);
  }

  /**
   * Toggles whether or not the region's information should be displayed.
   */
  toggleRegionInfo(): void {
    this.showRegionInfo = !this.showRegionInfo;
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Re-writes the games versions name.
   *
   * @param     { string }      name
   * @returns   { any }
   */
  private processVersionsNames(name: string): any {
    if ((name.match(/-/g) || []).length > 1 === true) {
      const splitNames = name.split('-');
      return [`${splitNames[0]}-${splitNames[1]}`, `${splitNames[2]}-${splitNames[3]}`];
    } else {
      return name.split('-');
    }
  }
}
