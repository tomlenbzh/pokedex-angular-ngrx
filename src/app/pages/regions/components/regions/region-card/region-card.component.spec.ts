import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SharedModule } from 'src/app/shared/shared.module';
import { RegionCardComponent } from './region-card.component';

describe('RegionCardComponent', () => {
  let component: RegionCardComponent;
  let fixture: ComponentFixture<RegionCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegionCardComponent],
      imports: [SharedModule, BrowserAnimationsModule, LazyLoadImageModule]
    }).compileComponents();

    fixture = TestBed.createComponent(RegionCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
