import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegionsRoutingModule } from './regions-routing.module';
import { containers } from './containers';
import { components } from './components';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModules } from 'src/app/shared/modules/material.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  declarations: [...containers, ...components],
  imports: [CommonModule, RegionsRoutingModule, SharedModule, LazyLoadImageModule, ...MaterialModules]
})
export class RegionsModule {}
