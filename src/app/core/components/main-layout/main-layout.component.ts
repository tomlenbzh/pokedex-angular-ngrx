import { Component, Input, SimpleChanges, OnChanges, ViewChild } from '@angular/core';
import { MatDrawerMode, MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnChanges {
  @Input() isOnTop: boolean = true;
  @Input() isLargeViewport: boolean = true;

  @ViewChild('sidenav') sidenav!: MatSidenav;

  fixed: boolean = true;
  opened: boolean = false;
  top: number = 0;
  bottom: number = 0;
  mode: MatDrawerMode = 'over';

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('isLargeViewport' in changes) {
      if (this.isLargeViewport && this.sidenav?.opened) this.toggleSidenav();
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * Opens / closes sidenav.
   */
  toggleSidenav(): void {
    this.opened = !this.opened;
  }
}
