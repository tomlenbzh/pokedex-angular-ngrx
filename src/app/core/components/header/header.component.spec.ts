import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { SharedModule } from 'src/app/shared/shared.module';
import { setMatIcons } from 'src/app/utils/methods/set-icons';
import { HeaderComponent } from './header.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let domSanitizer: DomSanitizer;
  let matIconRegistry: MatIconRegistry;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      imports: [SharedModule, HttpClientModule, RouterTestingModule, BrowserAnimationsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;

    domSanitizer = TestBed.inject(DomSanitizer);
    matIconRegistry = TestBed.inject(MatIconRegistry);
    setMatIcons(matIconRegistry, domSanitizer);

    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
