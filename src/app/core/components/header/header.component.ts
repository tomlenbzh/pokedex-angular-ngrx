import { Component, Input, EventEmitter, Output } from '@angular/core';
import { MenuItems } from 'src/app/utils/constants/menu.constants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Input() isOnTop: boolean = true;
  @Input() opened: boolean = true;

  @Output() toggled: EventEmitter<any> = new EventEmitter<any>();

  logo = 'assets/images/Pokeball-Home.png';
  menuItems = MenuItems;

  // -----------------------------------------------------------------------------------------------------
  // @ Public methds
  // -----------------------------------------------------------------------------------------------------

  /**
   * Emits to the parent component when the menu should be opened / closed.
   */
  toggleMenu(): void {
    this.toggled.emit();
  }
}
