import { Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItems } from 'src/app/utils/constants/menu.constants';

@Component({
  selector: 'app-sidenav-content',
  templateUrl: './sidenav-content.component.html',
  styleUrls: ['./sidenav-content.component.scss']
})
export class SidenavContentComponent {
  @Output() toggled: EventEmitter<any> = new EventEmitter<any>();

  menuItems = MenuItems;
  logo = 'assets/images/Pokemon Title.png';
  starters = 'assets/images/starters.png';

  constructor(private router: Router) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Emits to the parent component and navigates to the specified route.
   *
   * @param     { string }      url
   */
  navigateToPage(url: string): void {
    this.toggled.emit();
    this.router.navigateByUrl(url);
  }
}
