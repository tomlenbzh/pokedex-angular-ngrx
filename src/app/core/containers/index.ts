import { HeaderComponent } from '../components/header/header.component';
import { SidenavContentComponent } from '../components/sidenav-content/sidenav-content.component';
import { MainLayoutContainerComponent } from './main-layout-container/main-layout-container.component';

export const containers = [MainLayoutContainerComponent, HeaderComponent, SidenavContentComponent];
