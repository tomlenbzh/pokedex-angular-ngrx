import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, AfterViewInit, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-layout-container',
  template: '<app-main-layout [isOnTop]="isOnTop" [isLargeViewport]="isLargeViewport"></app-main-layout>'
})
export class MainLayoutContainerComponent implements AfterViewInit, OnDestroy, OnInit {
  isOnTop = true;
  isLargeViewport = false;

  private smallBreakPoint = '(min-width: 768px)';

  constructor(public breakpointObserver: BreakpointObserver) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit() {
    this.breakpointObserver
      .observe([this.smallBreakPoint])
      .subscribe((state: BreakpointState) => (this.isLargeViewport = state.matches));
  }

  ngAfterViewInit(): void {
    window.addEventListener('scroll', this.onScroll.bind(this), true);
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.onScroll, true);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Checks if has scrolled pas 50px in viewport.
   *
   * @param       { any }      event
   */
  private onScroll(event: any): void {
    if (event.srcElement.scrollTop > 50) {
      this.isOnTop = false;
    } else {
      this.isOnTop = true;
    }
  }
}
