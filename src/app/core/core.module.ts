import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from '../app.component';
import { components } from './components';
import { containers } from './containers';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { environment } from 'src/environments/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule, RouterState } from '@ngrx/router-store';
import { reducers, metaReducers } from '../store';
import { PokemonsListEffects } from '../store/pokemons/list/pokemons-list.effects';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { setMatIcons } from '../utils/methods/set-icons';
import { MatIconRegistry } from '@angular/material/icon';
import { SharedModule } from '../shared/shared.module';
import { PokemonsSingleEffects } from '../store/pokemons/single/pokemons-single.effects';
import { EvolutionsEffects } from '../store/evolutions/evolutions.effects';
import { RegionsEffects } from '../store/regions/regions.effects';
import { ToastrModule } from 'ngx-toastr';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { PokemonsSearchEffects } from '../store/pokemons/search/pokemons-search.effects';
import { TypesListEffects } from '../store/types/list/types-list.effects';
import { TypeSingleEffects } from '../store/types/single/type-single.effects';

const StoreEffects = [
  PokemonsListEffects,
  PokemonsSearchEffects,
  PokemonsSingleEffects,
  RegionsEffects,
  EvolutionsEffects,
  TypesListEffects,
  TypeSingleEffects
];

@NgModule({
  declarations: [AppComponent, ...components, ...containers],
  imports: [
    CommonModule,
    BrowserModule.withServerTransition({ appId: 'app' }),
    BrowserAnimationsModule,
    RouterModule,
    HttpClientModule,
    NgbModule,
    ToastrModule.forRoot(),
    ScrollToModule.forRoot(),
    SharedModule,
    LazyLoadImageModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([...StoreEffects]),
    StoreRouterConnectingModule.forRoot({ routerState: RouterState.Minimal })
  ]
})
export class CoreModule {
  constructor(
    @Optional() @SkipSelf() parentModule: CoreModule,
    private domSanitizer: DomSanitizer,
    private matIconRegistry: MatIconRegistry
  ) {
    if (parentModule) throw new Error('CoreModule is already loaded. Import it in the AppModule only');

    setMatIcons(this.matIconRegistry, this.domSanitizer);
  }

  static forRoot(): ModuleWithProviders<any> {
    return { ngModule: CoreModule, providers: [] };
  }
}
