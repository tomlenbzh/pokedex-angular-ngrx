export const MenuItems = [
  { label: 'Home', url: '/' },
  { label: 'Pokémons', url: '/pokemons' },
  { label: 'Evolutions', url: '/evolutions' },
  { label: 'Types', url: '/types' },
  { label: 'Regions', url: '/regions' }
];
