import { ITypeInfo } from '../models/pokemon.interface';

export const ALL_TYPES: ITypeInfo[] = [
  { label: 'Normal', class: 'type-normal' },
  { label: 'Fire', class: 'type-fire' },
  { label: 'Fighting', class: 'type-fighting' },
  { label: 'Water', class: 'type-water' },
  { label: 'Flying', class: 'type-flying' },
  { label: 'Grass', class: 'type-grass' },
  { label: 'Poison', class: 'type-poison' },
  { label: 'Electric', class: 'type-electric' },
  { label: 'Ground', class: 'type-ground' },
  { label: 'Psychic', class: 'type-psychic' },
  { label: 'Rock', class: 'type-rock' },
  { label: 'Ice', class: 'type-ice' },
  { label: 'Bug', class: 'type-bug' },
  { label: 'Dragon', class: 'type-dragon' },
  { label: 'Ghost', class: 'type-ghost' },
  { label: 'Dark', class: 'type-dark' },
  { label: 'Steel', class: 'type-steel' },
  { label: 'Fairy', class: 'type-fairy' },
  { label: '???', class: 'type-unknown' },
  { label: 'shadow', class: 'type-shadow' }
];

export const TypesGif: string[] = [
  'Water',
  'Flying',
  'Ground',
  'Electric',
  'Fairy',
  'Fire',
  'Ghost',
  'Grass',
  'Poison',
  'Bug',
  'Fighting',
  'Ice',
  'Rock'
];
