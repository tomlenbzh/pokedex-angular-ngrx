export const HomeSections = [
  {
    id: 'pokemons',
    title: 'Pokémons',
    subtitle: `Browse through your favourite Pokémons`,
    route: 'pokemons',
    bg: 'assets/images/boxes/pokemons-background.png'
  },
  {
    id: 'evolutions',
    title: 'Evolutions',
    subtitle: `Discover your Pokémons' evolutions`,
    route: 'evolutions',
    bg: 'assets/images/boxes/evolutions-background.png'
  },
  {
    id: 'types',
    title: 'Types',
    subtitle: `Discover the type of each Pokémon`,
    route: 'types',
    bg: 'assets/images/boxes/types.png'
  },
  {
    regions: 'regions',
    title: 'Regions',
    subtitle: `Discover the world of Pokémon`,
    route: 'regions',
    bg: 'assets/images/boxes/regions-background.png'
  }
];
