import { ALL_TYPES } from '../constants/types.constants';
import { IPokemon, IPokemonType, ITypeInfo } from '../models/pokemon.interface';

export const processPokemonInfo = (pokemon: IPokemon): any => {
  return {
    id: pokemon.id,
    name: pokemon.name,
    types: pokemon.types,
    image: getPokemonImage(pokemon),
    typesInfo: processTypes(pokemon.types[0].type)
  };
};

export const processTypes = (type: IPokemonType): ITypeInfo[] => {
  return ALL_TYPES.filter((item) => item.label?.toLocaleLowerCase() === type.name?.toLocaleLowerCase());
};

export const getPokemonImage = (pokemon: IPokemon) => {
  const paddedId = `${pokemon.id}`.padStart(3, '0');
  return pokemon.id > 905
    ? pokemon.sprites.front_default
    : `https://assets.pokemon.com/assets/cms2/img/pokedex/full/${paddedId}.png`;
};

export const getPokemonBasicInformation = (pokemon: IPokemon) => {
  return {
    id: pokemon?.id,
    name: pokemon?.name,
    types: pokemon?.types,
    height: pokemon?.height,
    weight: pokemon?.weight,
    baseExp: pokemon?.base_experience,
    stats: pokemon?.stats,
    sprites: pokemon?.sprites,
    image: getPokemonImage(pokemon),
    color: processTypes(pokemon.types[0].type)
  };
};
