export const buildQueryParams = (params: any): string => {
  const query = [];
  for (let param in params) {
    if (param) query.push(`${encodeURIComponent(param)}=${encodeURIComponent(params[param])}`);
  }

  return `?${query.toString().replace(/,/g, '&')}`;
};
