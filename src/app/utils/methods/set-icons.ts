import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import {
  featherIcons,
  heroicOutline,
  heroicSolid,
  matOutlineIcons,
  matSolidIcons,
  matTwoToneIcons
} from 'src/app/utils/constants/icons';

export const setMatIcons = (matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) => {
  matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl(matTwoToneIcons));
  matIconRegistry.addSvgIconSetInNamespace('mat_outline', domSanitizer.bypassSecurityTrustResourceUrl(matOutlineIcons));
  matIconRegistry.addSvgIconSetInNamespace('mat_solid', domSanitizer.bypassSecurityTrustResourceUrl(matSolidIcons));
  matIconRegistry.addSvgIconSetInNamespace('feather', domSanitizer.bypassSecurityTrustResourceUrl(featherIcons));
  matIconRegistry.addSvgIconSetInNamespace(
    'heroicons_outline',
    domSanitizer.bypassSecurityTrustResourceUrl(heroicOutline)
  );
  matIconRegistry.addSvgIconSetInNamespace('heroicons_solid', domSanitizer.bypassSecurityTrustResourceUrl(heroicSolid));
};
