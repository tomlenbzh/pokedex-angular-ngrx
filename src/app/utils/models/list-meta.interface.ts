export interface IListMeta {
  count: number;
  next: string;
  previous: string;
}
