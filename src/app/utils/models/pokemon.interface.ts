import { IListMeta } from './list-meta.interface';

// -----------------------------------------------------------------------------------------------------
// @ API POKEMONS
// -----------------------------------------------------------------------------------------------------

export interface IPokemonType {
  name: string;
  url: string;
}

export interface IPokemon {
  id: number;
  name: string;
  base_experience: string;
  order: number;
  height: number;
  weight: number;
  types: any[];
  sprites: IPokemonSprites;
  abilities: any[];
  stats: any;
  moves: any[];
}

export interface IPokemonsList extends IListMeta {
  results: IPokemon[];
}

export interface ITypeInfo {
  label: string;
  class: string;
}

export interface IPokemonSprites {
  back_default: string;
  back_female: string;
  back_shiny: string;
  back_shiny_female: string;
  front_default: string;
  front_female: string;
  front_shiny: string;
  front_shiny_female: string;
}
