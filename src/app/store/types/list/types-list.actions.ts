import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import { TypesListActionTypes } from './types-list.actions.types';

/**
 * Fetch Types list actions.
 */
export const FETCH_TYPES_LIST_ACTION = createAction(TypesListActionTypes.FETCH_TYPES_LIST);

export const FETCH_TYPES_LIST_SUCCESS_ACTION = createAction(
  TypesListActionTypes.FETCH_TYPES_LIST_SUCCESS,
  props<{ types: any[]; meta: IListMeta }>()
);

export const FETCH_TYPES_LIST_ERROR_ACTION = createAction(
  TypesListActionTypes.FETCH_TYPES_LIST_ERROR,
  props<{ error: HttpErrorResponse }>()
);
