import { createSelector } from '@ngrx/store';
import { AppState } from 'src/app/store';
import { TypesListState } from './types-list.reducer';

export const TypesListFeature = (state: AppState) => state.typesList;

export const selectTypesList = createSelector(TypesListFeature, (state: TypesListState) => state.types);
export const selectTypesListMeta = createSelector(TypesListFeature, (state: TypesListState) => state.meta);
export const selectTypesListLoading = createSelector(TypesListFeature, (state: TypesListState) => state.isLoading);
