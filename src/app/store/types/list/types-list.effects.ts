import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, map, Observable, of } from 'rxjs';
import { ToasterService } from 'src/app/services/toaster.service';
import { TypesService } from 'src/app/services/types.service';
import {
  FETCH_TYPES_LIST_ACTION,
  FETCH_TYPES_LIST_ERROR_ACTION,
  FETCH_TYPES_LIST_SUCCESS_ACTION
} from './types-list.actions';

@Injectable()
export class TypesListEffects {
  constructor(
    private actions$: Actions,
    private typesService: TypesService,
    private toasterService: ToasterService,
    private router: Router
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Store Effects
  // -----------------------------------------------------------------------------------------------------

  fetchTypesList$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_TYPES_LIST_ACTION),
      exhaustMap((action) => {
        return this.typesService.fetchAllTypes().pipe(
          map((list: any) => {
            const types: any[] = list?.results;
            const { results, ...meta } = list;

            return FETCH_TYPES_LIST_SUCCESS_ACTION({ types, meta });

            // return forkJoin(
            //   raw.map((rawPokemon) =>
            //     this.pokemonsService
            //       .fetchPokemonInformation(rawPokemon)
            //       .pipe(map((pokemon: IPokemon) => processPokemonInfo(pokemon)))
            //   )
            // ).pipe(map((pokemons: any[]) => FETCH_POKEMONS_LIST_SUCCESS_ACTION({ pokemons, meta })));
          }),
          catchError((error) => this.handleError(error, 'Error', 'An error occured while fetching the Pokémons list'))
        );
      })
    );
  });

  private handleError(error: HttpErrorResponse, title: string, message: string): Observable<any> {
    this.toasterService.error(title, message);
    this.router.navigateByUrl('');
    return of(FETCH_TYPES_LIST_ERROR_ACTION({ error }));
  }
}
