import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import { FETCH_TYPES_LIST_ACTION } from './types-list.actions';
import { selectTypesList, selectTypesListLoading, selectTypesListMeta } from './types-list.selectors';

@Injectable({ providedIn: 'root' })
export class TypesListHelper {
  constructor(private store: Store<AppState>) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Selectors
  // -----------------------------------------------------------------------------------------------------

  isLoading(): Observable<boolean> {
    return this.store.pipe(select(selectTypesListLoading));
  }

  typesList(): Observable<any[] | null> {
    return this.store.select(selectTypesList);
  }

  meta(): Observable<IListMeta | null> {
    return this.store.select(selectTypesListMeta);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Store Actions
  // -----------------------------------------------------------------------------------------------------

  /**
   * Fetches a list of Pokémons types.
   */
  fetchTypesList(): void {
    this.store.dispatch(FETCH_TYPES_LIST_ACTION());
  }
}
