export enum TypesListActionTypes {
  FETCH_TYPES_LIST = '[TYPES LIST] Fetch Pokémons types list',
  FETCH_TYPES_LIST_SUCCESS = '[TYPES LIST] Fetch Pokémons types list Success',
  FETCH_TYPES_LIST_ERROR = '[TYPES LIST] Fetch Pokémons types list Error'
}
