import { createReducer, on } from '@ngrx/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import {
  FETCH_TYPES_LIST_ACTION,
  FETCH_TYPES_LIST_ERROR_ACTION,
  FETCH_TYPES_LIST_SUCCESS_ACTION
} from './types-list.actions';

export interface TypesListState {
  types: any[];
  meta: IListMeta | null;
  isLoading: boolean;
}

export const initialState: TypesListState = {
  types: [],
  meta: null,
  isLoading: false
};

export const typesListReducer = createReducer(
  initialState,
  on(FETCH_TYPES_LIST_ACTION, (state: TypesListState): TypesListState => ({ ...state, isLoading: true })),
  on(FETCH_TYPES_LIST_ERROR_ACTION, (state: TypesListState): TypesListState => ({ ...state, isLoading: false })),
  on(
    FETCH_TYPES_LIST_SUCCESS_ACTION,
    (state: TypesListState, { types, meta }): TypesListState => ({ types, meta, isLoading: false })
  )
);
