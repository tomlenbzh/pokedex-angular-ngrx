import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store';
import { FETCH_TYPE_SINGLE_ACTION } from './type-single.actions';
import { selectTypeSingle, selectTypeSingleLoading } from './type-single.selectors';

@Injectable({ providedIn: 'root' })
export class TypeSingleHelper {
  constructor(private store: Store<AppState>) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Selectors
  // -----------------------------------------------------------------------------------------------------

  isLoading(): Observable<boolean> {
    return this.store.pipe(select(selectTypeSingleLoading));
  }

  typeDetails(): Observable<any> {
    return this.store.select(selectTypeSingle);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Store Actions
  // -----------------------------------------------------------------------------------------------------

  /**
   * Fetches the selected type's details regarding its name.
   *
   * @param     { string }      name
   */
  fetchTypeDetail(name: string): void {
    this.store.dispatch(FETCH_TYPE_SINGLE_ACTION({ name }));
  }
}
