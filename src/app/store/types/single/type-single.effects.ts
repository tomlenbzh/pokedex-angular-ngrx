import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, map, Observable, of } from 'rxjs';
import { ToasterService } from 'src/app/services/toaster.service';
import { TypesService } from 'src/app/services/types.service';
import {
  FETCH_TYPE_SINGLE_ACTION,
  FETCH_TYPE_SINGLE_ERROR_ACTION,
  FETCH_TYPE_SINGLE_SUCCESS_ACTION
} from './type-single.actions';

@Injectable()
export class TypeSingleEffects {
  constructor(
    private actions$: Actions,
    private typesService: TypesService,
    private toasterService: ToasterService,
    private router: Router
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Store Effects
  // -----------------------------------------------------------------------------------------------------

  fetchTypeDetail = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_TYPE_SINGLE_ACTION),
      exhaustMap((action) => {
        const { name } = action;
        return this.typesService.fetchType(name).pipe(
          map((typeDetails) => FETCH_TYPE_SINGLE_SUCCESS_ACTION({ typeDetails })),
          catchError((error) => this.handleError(error, 'Error', 'An error occured while fetching this type'))
        );
      })
    );
  });

  private handleError(error: HttpErrorResponse, title: string, message: string): Observable<any> {
    this.toasterService.error(title, message);
    this.router.navigateByUrl('');
    return of(FETCH_TYPE_SINGLE_ERROR_ACTION({ error }));
  }
}
