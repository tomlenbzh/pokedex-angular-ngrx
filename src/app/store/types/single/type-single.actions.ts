import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { TypeSingleActionTypes } from './type-single.actions.types';

/**
 * Fetch Type details actions.
 */
export const FETCH_TYPE_SINGLE_ACTION = createAction(
  TypeSingleActionTypes.FETCH_TYPE_SINGLE,
  props<{ name: string }>()
);

export const FETCH_TYPE_SINGLE_SUCCESS_ACTION = createAction(
  TypeSingleActionTypes.FETCH_TYPE_SINGLE_SUCCESS,
  props<{ typeDetails: any }>()
);

export const FETCH_TYPE_SINGLE_ERROR_ACTION = createAction(
  TypeSingleActionTypes.FETCH_TYPE_SINGLE_ERROR,
  props<{ error: HttpErrorResponse }>()
);
