export enum TypeSingleActionTypes {
  FETCH_TYPE_SINGLE = '[TYPE SINGLE] Fetch Type single',
  FETCH_TYPE_SINGLE_SUCCESS = '[TYPE SINGLE] Fetch Type single Success',
  FETCH_TYPE_SINGLE_ERROR = '[TYPE SINGLE] Fetch Type single Error'
}
