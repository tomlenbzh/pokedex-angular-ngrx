import { createReducer, on } from '@ngrx/store';
import {
  FETCH_TYPE_SINGLE_ACTION,
  FETCH_TYPE_SINGLE_SUCCESS_ACTION,
  FETCH_TYPE_SINGLE_ERROR_ACTION
} from './type-single.actions';

export interface TypeSingleState {
  typeDetails: any;
  isLoading: boolean;
}

export const initialState: TypeSingleState = {
  typeDetails: null,
  isLoading: false
};

export const typeSingleReducer = createReducer(
  initialState,
  on(FETCH_TYPE_SINGLE_ACTION, (state: TypeSingleState): TypeSingleState => ({ ...state, isLoading: true })),
  on(FETCH_TYPE_SINGLE_ERROR_ACTION, (state: TypeSingleState): TypeSingleState => ({ ...state, isLoading: false })),
  on(FETCH_TYPE_SINGLE_SUCCESS_ACTION, (state, { typeDetails }): TypeSingleState => ({ typeDetails, isLoading: false }))
);
