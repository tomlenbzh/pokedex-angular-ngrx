import { createSelector } from '@ngrx/store';
import { AppState } from 'src/app/store';
import { TypeSingleState } from './type-single.reducer';

export const TypeSingleFeature = (state: AppState) => state.typesSingle;

export const selectTypeSingle = createSelector(TypeSingleFeature, (state: TypeSingleState) => state.typeDetails);
export const selectTypeSingleLoading = createSelector(TypeSingleFeature, (state: TypeSingleState) => state.isLoading);
