import { createReducer, on } from '@ngrx/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import {
  FETCH_REGIONS_LIST_ACTION,
  FETCH_REGIONS_LIST_ERROR_ACTION,
  FETCH_REGIONS_LIST_SUCCESS_ACTION
} from './regions.actions';

export interface RegionsState {
  regions: any[];
  meta: IListMeta | null;
  isLoading: boolean;
}

export const initialState: RegionsState = {
  regions: [],
  meta: null,
  isLoading: false
};

export const regionsReducer = createReducer(
  initialState,
  on(FETCH_REGIONS_LIST_ACTION, (state: RegionsState): RegionsState => ({ ...state, isLoading: true })),
  on(FETCH_REGIONS_LIST_ERROR_ACTION, (state: RegionsState): RegionsState => ({ ...state, isLoading: false })),
  on(
    FETCH_REGIONS_LIST_SUCCESS_ACTION,
    (state: RegionsState, { regions, meta }): RegionsState => ({ regions, meta, isLoading: false })
  )
);
