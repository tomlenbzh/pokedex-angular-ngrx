import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import { RegionsActionTypes } from './regions.actions.types';

/**
 * Fetch Regions list actions.
 */
export const FETCH_REGIONS_LIST_ACTION = createAction(RegionsActionTypes.FETCH_REGIONS_LIST);

export const FETCH_REGIONS_LIST_SUCCESS_ACTION = createAction(
  RegionsActionTypes.FETCH_REGIONS_LIST_SUCCESS,
  props<{ regions: any[]; meta: IListMeta }>()
);

export const FETCH_REGIONS_LIST_ERROR_ACTION = createAction(
  RegionsActionTypes.FETCH_REGIONS_LIST_ERROR,
  props<{ error: HttpErrorResponse }>()
);
