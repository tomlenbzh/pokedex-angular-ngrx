import { createSelector } from '@ngrx/store';
import { AppState } from 'src/app/store';
import { RegionsState } from './regions.reducer';

export const RegionsFeature = (state: AppState) => state.regions;

export const selectRegionsList = createSelector(RegionsFeature, (state: RegionsState) => state.regions);
export const selectRegionsMeta = createSelector(RegionsFeature, (state: RegionsState) => state.meta);
export const selectRegionsLoading = createSelector(RegionsFeature, (state: RegionsState) => state.isLoading);
