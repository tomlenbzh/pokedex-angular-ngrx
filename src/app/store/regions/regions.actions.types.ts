export enum RegionsActionTypes {
  FETCH_REGIONS_LIST = '[REGIONS LIST] Fetch Regions list',
  FETCH_REGIONS_LIST_SUCCESS = '[REGIONS LIST] Fetch Regions list Success',
  FETCH_REGIONS_LIST_ERROR = '[REGIONS LIST] Fetch Regions list Error'
}
