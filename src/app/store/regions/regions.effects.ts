import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, forkJoin, map, mergeMap, Observable, of } from 'rxjs';
import { RegionsService } from 'src/app/services/regions.service';
import { ToasterService } from 'src/app/services/toaster.service';
import {
  FETCH_REGIONS_LIST_ACTION,
  FETCH_REGIONS_LIST_ERROR_ACTION,
  FETCH_REGIONS_LIST_SUCCESS_ACTION
} from './regions.actions';

@Injectable()
export class RegionsEffects {
  constructor(
    private actions$: Actions,
    private regionsService: RegionsService,
    private toasterService: ToasterService,
    private router: Router
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Store Effects
  // -----------------------------------------------------------------------------------------------------

  fetchPokemonDetail = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_REGIONS_LIST_ACTION),
      exhaustMap(() => {
        return this.regionsService.fetchAllRegions().pipe(
          mergeMap((list: any) => {
            const raw: any[] = list?.results;
            const { results, ...meta } = list;

            return forkJoin(
              raw.map((rawRegion) => this.regionsService.fetchRegion(rawRegion.name).pipe(map((pokemon) => pokemon)))
            ).pipe(map((regions: any[]) => FETCH_REGIONS_LIST_SUCCESS_ACTION({ regions, meta })));
          }),
          catchError((error) => this.handleError(error, 'Error', 'An error occured while fetching this Pokémon'))
        );
      })
    );
  });

  private handleError(error: HttpErrorResponse, title: string, message: string): Observable<any> {
    this.toasterService.error(title, message);
    this.router.navigateByUrl('');
    return of(FETCH_REGIONS_LIST_ERROR_ACTION({ error }));
  }
}
