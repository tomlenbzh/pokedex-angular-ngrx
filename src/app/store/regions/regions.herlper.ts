import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import { FETCH_REGIONS_LIST_ACTION } from './regions.actions';
import { selectRegionsList, selectRegionsLoading, selectRegionsMeta } from './regions.selectors';

@Injectable({ providedIn: 'root' })
export class RegionsListHelper {
  constructor(private store: Store<AppState>) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Selectors
  // -----------------------------------------------------------------------------------------------------

  isLoading(): Observable<boolean | null> {
    return this.store.pipe(select(selectRegionsLoading));
  }

  regionsList(): Observable<any[] | null> {
    return this.store.select(selectRegionsList);
  }

  meta(): Observable<IListMeta | null> {
    return this.store.select(selectRegionsMeta);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Store Actions
  // -----------------------------------------------------------------------------------------------------

  /**
   * Fetches the full list of Pokémon regions.
   */
  fetchPokemonsList(): void {
    this.store.dispatch(FETCH_REGIONS_LIST_ACTION());
  }
}
