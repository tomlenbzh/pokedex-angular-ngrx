import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, forkJoin, map, mergeMap, Observable, of } from 'rxjs';
import { PokemonsService } from 'src/app/services/pokémons.service';
import { ToasterService } from 'src/app/services/toaster.service';
import { getPokemonBasicInformation } from 'src/app/utils/methods/process-pokemon-info';
import { IPokemon } from 'src/app/utils/models/pokemon.interface';
import {
  FETCH_POKEMONS_SINGLE_ACTION,
  FETCH_POKEMONS_SINGLE_SUCCESS_ACTION,
  FETCH_POKEMONS_SINGLE_ERROR_ACTION
} from './pokemons-single.actions';

@Injectable()
export class PokemonsSingleEffects {
  constructor(
    private actions$: Actions,
    private pokemonsService: PokemonsService,
    private toasterService: ToasterService,
    private router: Router
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Store Effects
  // -----------------------------------------------------------------------------------------------------

  fetchPokemonDetail = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_POKEMONS_SINGLE_ACTION),
      exhaustMap((action) => {
        const { id } = action;
        let basicInfo: any = null;
        let abilities: any[] = [];
        let moves: any[] = [];

        return this.pokemonsService.fetchPokemonDetailById(id).pipe(
          mergeMap((rawData: IPokemon) => {
            basicInfo = getPokemonBasicInformation(rawData);
            return forkJoin(
              rawData?.abilities.map((rawAbility) => {
                return this.pokemonsService.fetchPokemonAbility(rawAbility?.ability?.url).pipe(
                  mergeMap((abilityRes) => {
                    const ability = { name: rawAbility?.ability?.name, detail: abilityRes };
                    abilities.push(ability);

                    return forkJoin(
                      rawData?.moves.map((rawMove) => {
                        return this.pokemonsService.fetchPokemonMove(rawMove?.move?.url).pipe(
                          map((moveRes) => {
                            const move = { name: rawMove?.move?.name, detail: moveRes };
                            moves.push(move);
                          }),
                          catchError((error) =>
                            this.handleError(error, 'Error', 'An error occured while fetching this Pokémon')
                          )
                        );
                      })
                    );
                  }),
                  catchError((error) =>
                    this.handleError(error, 'Error', 'An error occured while fetching this Pokémon')
                  )
                );
              })
            ).pipe(map(() => FETCH_POKEMONS_SINGLE_SUCCESS_ACTION({ pokemon: { basicInfo, abilities, moves } })));
          }),
          catchError((error) => this.handleError(error, 'Error', 'An error occured while fetching this Pokémon'))
        );
      })
    );
  });

  private handleError(error: HttpErrorResponse, title: string, message: string): Observable<any> {
    this.toasterService.error(title, message);
    this.router.navigateByUrl('');
    return of(FETCH_POKEMONS_SINGLE_ERROR_ACTION({ error }));
  }
}
