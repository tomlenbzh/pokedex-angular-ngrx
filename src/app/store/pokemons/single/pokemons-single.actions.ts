import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { PokemonsSingleActionTypes } from './pokemons-single.actions.types';

/**
 * Fetch Pokémons list actions.
 */
export const FETCH_POKEMONS_SINGLE_ACTION = createAction(
  PokemonsSingleActionTypes.FETCH_POKEMONS_SINGLE,
  props<{ id: number }>()
);

export const FETCH_POKEMONS_SINGLE_SUCCESS_ACTION = createAction(
  PokemonsSingleActionTypes.FETCH_POKEMONS_SINGLE_SUCCESS,
  props<{ pokemon: any }>()
);

export const FETCH_POKEMONS_SINGLE_ERROR_ACTION = createAction(
  PokemonsSingleActionTypes.FETCH_POKEMONS_SINGLE_ERROR,
  props<{ error: HttpErrorResponse }>()
);
