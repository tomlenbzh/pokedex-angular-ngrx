import { createReducer, on } from '@ngrx/store';
import {
  FETCH_POKEMONS_SINGLE_ACTION,
  FETCH_POKEMONS_SINGLE_SUCCESS_ACTION,
  FETCH_POKEMONS_SINGLE_ERROR_ACTION
} from './pokemons-single.actions';

export interface PokemonsSingleState {
  pokemon: any;
  isLoading: boolean;
}

export const initialState: PokemonsSingleState = {
  pokemon: null,
  isLoading: false
};

export const pokemonsSingleReducer = createReducer(
  initialState,
  on(
    FETCH_POKEMONS_SINGLE_ACTION,
    (state: PokemonsSingleState): PokemonsSingleState => ({ ...state, isLoading: true })
  ),
  on(
    FETCH_POKEMONS_SINGLE_ERROR_ACTION,
    (state: PokemonsSingleState): PokemonsSingleState => ({ ...state, isLoading: false })
  ),
  on(
    FETCH_POKEMONS_SINGLE_SUCCESS_ACTION,
    (state: PokemonsSingleState, { pokemon }): PokemonsSingleState => ({ pokemon, isLoading: false })
  )
);
