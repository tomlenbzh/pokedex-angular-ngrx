export enum PokemonsSingleActionTypes {
  FETCH_POKEMONS_SINGLE = '[POKEMONS SINGLE] Fetch Pokémons single',
  FETCH_POKEMONS_SINGLE_SUCCESS = '[POKEMONS SINGLE] Fetch Pokémons single Success',
  FETCH_POKEMONS_SINGLE_ERROR = '[POKEMONS SINGLE] Fetch Pokémons single Error'
}
