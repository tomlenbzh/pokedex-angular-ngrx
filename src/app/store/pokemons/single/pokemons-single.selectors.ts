import { createSelector } from '@ngrx/store';
import { AppState } from 'src/app/store';
import { PokemonsSingleState } from './pokemons-single.reducer';

export const PokemonsSingleFeature = (state: AppState) => state.pokemonsSingle;

export const selectPokemonsSingle = createSelector(
  PokemonsSingleFeature,
  (state: PokemonsSingleState) => state.pokemon
);
export const selectPokemonsSingleLoading = createSelector(
  PokemonsSingleFeature,
  (state: PokemonsSingleState) => state.isLoading
);
