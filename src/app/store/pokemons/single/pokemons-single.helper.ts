import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store';
import { FETCH_POKEMONS_SINGLE_ACTION } from './pokemons-single.actions';
import { selectPokemonsSingle, selectPokemonsSingleLoading } from './pokemons-single.selectors';

@Injectable({ providedIn: 'root' })
export class PokemonsSingleHelper {
  constructor(private store: Store<AppState>) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Selectors
  // -----------------------------------------------------------------------------------------------------

  isLoading(): Observable<boolean> {
    return this.store.pipe(select(selectPokemonsSingleLoading));
  }

  pokemon(): Observable<any> {
    return this.store.select(selectPokemonsSingle);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Store Actions
  // -----------------------------------------------------------------------------------------------------

  /**
   * Fetches the selected Pokémon's details regarding its id.
   *
   * @param     { number }      id
   */
  fetchPokemonDetail(id: number): void {
    this.store.dispatch(FETCH_POKEMONS_SINGLE_ACTION({ id }));
  }
}
