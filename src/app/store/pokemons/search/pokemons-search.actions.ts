import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import { PokemonsSearchActionTypes } from './pokemons-search.actions.types';

/**
 * Fetch Pokémons search actions.
 */
export const FETCH_POKEMONS_SEARCH_ACTION = createAction(PokemonsSearchActionTypes.FETCH_POKEMONS_SEARCH);

export const FETCH_POKEMONS_SEARCH_SUCCESS_ACTION = createAction(
  PokemonsSearchActionTypes.FETCH_POKEMONS_SEARCH_SUCCESS,
  props<{ pokemons: any[]; meta: IListMeta }>()
);

export const FETCH_POKEMONS_SEARCH_ERROR_ACTION = createAction(
  PokemonsSearchActionTypes.FETCH_POKEMONS_SEARCH_SUCCESS,
  props<{ error: HttpErrorResponse }>()
);
