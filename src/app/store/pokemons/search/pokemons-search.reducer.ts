import { createReducer, on } from '@ngrx/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import {
  FETCH_POKEMONS_SEARCH_ACTION,
  FETCH_POKEMONS_SEARCH_SUCCESS_ACTION,
  FETCH_POKEMONS_SEARCH_ERROR_ACTION
} from './pokemons-search.actions';

export interface PokemonsSearchState {
  pokemons: any[];
  meta: IListMeta | null;
  isLoading: boolean;
}

export const initialState: PokemonsSearchState = {
  pokemons: [],
  meta: null,
  isLoading: false
};

export const pokemonsSearchReducer = createReducer(
  initialState,
  on(
    FETCH_POKEMONS_SEARCH_ACTION,
    (state: PokemonsSearchState): PokemonsSearchState => ({ ...state, isLoading: true })
  ),
  on(
    FETCH_POKEMONS_SEARCH_ERROR_ACTION,
    (state: PokemonsSearchState): PokemonsSearchState => ({ ...state, isLoading: false })
  ),
  on(
    FETCH_POKEMONS_SEARCH_SUCCESS_ACTION,
    (state: PokemonsSearchState, { pokemons, meta }): PokemonsSearchState => ({
      pokemons,
      meta,
      isLoading: false
    })
  )
);
