import { createSelector } from '@ngrx/store';
import { AppState } from 'src/app/store';
import { PokemonsSearchState } from './pokemons-search.reducer';

export const PokemonsListFeature = (state: AppState) => state.search;

export const selectPokemonsSearch = createSelector(PokemonsListFeature, (state: PokemonsSearchState) => state.pokemons);
export const selectPokemonsSearchMeta = createSelector(PokemonsListFeature, (state: PokemonsSearchState) => state.meta);
export const selectPokemonsSearchLoading = createSelector(
  PokemonsListFeature,
  (state: PokemonsSearchState) => state.isLoading
);
