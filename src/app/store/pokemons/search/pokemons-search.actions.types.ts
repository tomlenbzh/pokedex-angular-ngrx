export enum PokemonsSearchActionTypes {
  FETCH_POKEMONS_SEARCH = '[POKEMONS SEARCH] Fetch Pokémons search',
  FETCH_POKEMONS_SEARCH_SUCCESS = '[POKEMONS SEARCH] Fetch Pokémons search Success',
  FETCH_POKEMONS_SEARCH_ERROR = '[POKEMONS SEARCH] Fetch Pokémons search Error'
}
