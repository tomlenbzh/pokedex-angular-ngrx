import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import { FETCH_POKEMONS_SEARCH_ACTION } from './pokemons-search.actions';
import { selectPokemonsSearch, selectPokemonsSearchLoading, selectPokemonsSearchMeta } from './pokemons-selectors';

@Injectable({ providedIn: 'root' })
export class PokemonsSearchHelper {
  constructor(private store: Store<AppState>) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Selectors
  // -----------------------------------------------------------------------------------------------------

  isLoading(): Observable<boolean> {
    return this.store.pipe(select(selectPokemonsSearchLoading));
  }

  pokemonsSearch(): Observable<any[] | null> {
    return this.store.select(selectPokemonsSearch);
  }

  meta(): Observable<IListMeta | null> {
    return this.store.select(selectPokemonsSearchMeta);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Store Actions
  // -----------------------------------------------------------------------------------------------------

  /**
   * Fetches the full list of Pokémons.
   */
  fetchPokemonsSearch(): void {
    this.store.dispatch(FETCH_POKEMONS_SEARCH_ACTION());
  }
}
