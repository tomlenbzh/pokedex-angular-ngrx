import { createSelector } from '@ngrx/store';
import { AppState } from 'src/app/store';
import { PokemonsListState } from './pokemons-list.reducer';

export const PokemonsListFeature = (state: AppState) => state.pokemonsList;

export const selectPokemonsList = createSelector(PokemonsListFeature, (state: PokemonsListState) => state.pokemons);
export const selectPokemonsListMeta = createSelector(PokemonsListFeature, (state: PokemonsListState) => state.meta);
export const selectPokemonsListLoading = createSelector(
  PokemonsListFeature,
  (state: PokemonsListState) => state.isLoading
);
