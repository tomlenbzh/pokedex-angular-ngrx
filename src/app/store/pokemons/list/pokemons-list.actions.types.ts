export enum PokemonsListActionTypes {
  FETCH_POKEMONS_LIST = '[POKEMONS LIST] Fetch Pokémons list',
  FETCH_POKEMONS_LIST_SUCCESS = '[POKEMONS LIST] Fetch Pokémons list Success',
  FETCH_POKEMONS_LIST_ERROR = '[POKEMONS LIST] Fetch Pokémons list Error'
}
