import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import { FETCH_POKEMONS_LIST_ACTION } from './pokemons-list.actions';
import { selectPokemonsList, selectPokemonsListLoading, selectPokemonsListMeta } from './pokemons-list.selectors';

@Injectable({ providedIn: 'root' })
export class PokemonsListHelper {
  constructor(private store: Store<AppState>) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Selectors
  // -----------------------------------------------------------------------------------------------------

  isLoading(): Observable<boolean> {
    return this.store.pipe(select(selectPokemonsListLoading));
  }

  pokemonsList(): Observable<any[] | null> {
    return this.store.select(selectPokemonsList);
  }

  meta(): Observable<IListMeta | null> {
    return this.store.select(selectPokemonsListMeta);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Store Actions
  // -----------------------------------------------------------------------------------------------------

  /**
   * Fetches a list of Pokémons regarding the specified limit and offset parameters.
   *
   * @param     { number }      limit     // The max number of Pokémons to be returned
   * @param     { number }      offset    // The first element of the page.
   */
  fetchPokemonsList(limit: number, offset: number): void {
    this.store.dispatch(FETCH_POKEMONS_LIST_ACTION({ limit, offset }));
  }
}
