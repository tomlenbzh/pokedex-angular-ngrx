import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, forkJoin, map, mergeMap, Observable, of } from 'rxjs';
import { PokemonsService } from 'src/app/services/pokémons.service';
import { ToasterService } from 'src/app/services/toaster.service';
import { processPokemonInfo } from 'src/app/utils/methods/process-pokemon-info';
import { IPokemon } from 'src/app/utils/models/pokemon.interface';
import {
  FETCH_POKEMONS_LIST_ACTION,
  FETCH_POKEMONS_LIST_ERROR_ACTION,
  FETCH_POKEMONS_LIST_SUCCESS_ACTION
} from './pokemons-list.actions';

@Injectable()
export class PokemonsListEffects {
  constructor(
    private actions$: Actions,
    private pokemonsService: PokemonsService,
    private toasterService: ToasterService,
    private router: Router
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Store Effects
  // -----------------------------------------------------------------------------------------------------

  fetchPokemonsList$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_POKEMONS_LIST_ACTION),
      exhaustMap((action) => {
        return this.pokemonsService.fetchPokemonsList(action.limit, action.offset).pipe(
          mergeMap((list: any) => {
            const raw: any[] = list?.results;
            const { results, ...meta } = list;

            return forkJoin(
              raw.map((rawPokemon) =>
                this.pokemonsService
                  .fetchPokemonInformation(rawPokemon)
                  .pipe(map((pokemon: IPokemon) => processPokemonInfo(pokemon)))
              )
            ).pipe(map((pokemons: any[]) => FETCH_POKEMONS_LIST_SUCCESS_ACTION({ pokemons, meta })));
          }),
          catchError((error) => this.handleError(error, 'Error', 'An error occured while fetching the Pokémons list'))
        );
      })
    );
  });

  private handleError(error: HttpErrorResponse, title: string, message: string): Observable<any> {
    this.toasterService.error(title, message);
    this.router.navigateByUrl('');
    return of(FETCH_POKEMONS_LIST_ERROR_ACTION({ error }));
  }
}
