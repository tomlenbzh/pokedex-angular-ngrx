import { createReducer, on } from '@ngrx/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import {
  FETCH_POKEMONS_LIST_ACTION,
  FETCH_POKEMONS_LIST_ERROR_ACTION,
  FETCH_POKEMONS_LIST_SUCCESS_ACTION
} from './pokemons-list.actions';

export interface PokemonsListState {
  pokemons: any[];
  meta: IListMeta | null;
  isLoading: boolean;
}

export const initialState: PokemonsListState = {
  pokemons: [],
  meta: null,
  isLoading: false
};

export const pokemonsListReducer = createReducer(
  initialState,
  on(FETCH_POKEMONS_LIST_ACTION, (state: PokemonsListState): PokemonsListState => ({ ...state, isLoading: true })),
  on(
    FETCH_POKEMONS_LIST_ERROR_ACTION,
    (state: PokemonsListState): PokemonsListState => ({ ...state, isLoading: false })
  ),
  on(
    FETCH_POKEMONS_LIST_SUCCESS_ACTION,
    (state: PokemonsListState, { pokemons, meta }): PokemonsListState => ({ pokemons, meta, isLoading: false })
  )
);
