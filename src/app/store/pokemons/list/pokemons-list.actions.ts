import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import { PokemonsListActionTypes } from './pokemons-list.actions.types';

/**
 * Fetch Pokémons list actions.
 */
export const FETCH_POKEMONS_LIST_ACTION = createAction(
  PokemonsListActionTypes.FETCH_POKEMONS_LIST,
  props<{ limit?: number; offset?: number }>()
);

export const FETCH_POKEMONS_LIST_SUCCESS_ACTION = createAction(
  PokemonsListActionTypes.FETCH_POKEMONS_LIST_SUCCESS,
  props<{ pokemons: any[]; meta: IListMeta }>()
);

export const FETCH_POKEMONS_LIST_ERROR_ACTION = createAction(
  PokemonsListActionTypes.FETCH_POKEMONS_LIST_ERROR,
  props<{ error: HttpErrorResponse }>()
);
