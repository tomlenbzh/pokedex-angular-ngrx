import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from 'src/environments/environment';
import { PokemonsListState, pokemonsListReducer } from './pokemons/list/pokemons-list.reducer';
import { PokemonsSearchState, pokemonsSearchReducer } from './pokemons/search/pokemons-search.reducer';
import { PokemonsSingleState, pokemonsSingleReducer } from './pokemons/single/pokemons-single.reducer';
import { RegionsState, regionsReducer } from './regions/regions.reducer';
import { EvolutionsState, evolutionsListReducer } from './evolutions/evolutions.reducer';
import { TypesListState, typesListReducer } from './types/list/types-list.reducer';
import { TypeSingleState, typeSingleReducer } from './types/single/type-single.reducer';

export interface AppState {
  pokemonsList: PokemonsListState;
  pokemonsSingle: PokemonsSingleState;
  regions: RegionsState;
  evolutions: EvolutionsState;
  search: PokemonsSearchState;
  typesList: TypesListState;
  typesSingle: TypeSingleState;
}

export const reducers: ActionReducerMap<AppState> = {
  pokemonsList: pokemonsListReducer,
  pokemonsSingle: pokemonsSingleReducer,
  regions: regionsReducer,
  evolutions: evolutionsListReducer,
  search: pokemonsSearchReducer,
  typesList: typesListReducer,
  typesSingle: typeSingleReducer
};

export const metaReducers: MetaReducer<any>[] = !environment.production ? [] : [];
