import { createSelector } from '@ngrx/store';
import { AppState } from 'src/app/store';
import { EvolutionsState } from './evolutions.reducer';

export const EvolutionsFeature = (state: AppState) => state.evolutions;

export const selectEvolutions = createSelector(EvolutionsFeature, (state: EvolutionsState) => state.evolutions);
export const selectEvolutionsMeta = createSelector(EvolutionsFeature, (state: EvolutionsState) => state.meta);
export const selectEvolutionsLoading = createSelector(EvolutionsFeature, (state: EvolutionsState) => state.isLoading);
