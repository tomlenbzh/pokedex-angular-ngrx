import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import { EvolutionsListActionTypes } from './evolutions.actions.types';

/**
 * Fetch Evolutions list actions.
 */
export const FETCH_EVOLUTIONS_LIST_ACTION = createAction(
  EvolutionsListActionTypes.FETCH_EVOLUTIONS_LIST,
  props<{ limit?: number; offset?: number }>()
);

export const FETCH_EVOLUTIONS_LIST_SUCCESS_ACTION = createAction(
  EvolutionsListActionTypes.FETCH_EVOLUTIONS_LIST_SUCCESS,
  props<{ evolutions: any[]; meta: IListMeta }>()
);

export const FETCH_EVOLUTIONS_LIST_ERROR_ACTION = createAction(
  EvolutionsListActionTypes.FETCH_EVOLUTIONS_LIST_ERROR,
  props<{ error: HttpErrorResponse }>()
);
