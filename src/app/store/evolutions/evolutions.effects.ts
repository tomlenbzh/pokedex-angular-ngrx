import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, forkJoin, map, mergeMap, Observable, of, switchMap, tap } from 'rxjs';
import { EvolutionsService } from 'src/app/services/evolutions.service';
import { ToasterService } from 'src/app/services/toaster.service';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import {
  FETCH_EVOLUTIONS_LIST_ACTION,
  FETCH_EVOLUTIONS_LIST_SUCCESS_ACTION,
  FETCH_EVOLUTIONS_LIST_ERROR_ACTION
} from './evolutions.actions';

@Injectable()
export class EvolutionsEffects {
  constructor(
    private actions$: Actions,
    private evolutionsService: EvolutionsService,
    private toasterService: ToasterService,
    private router: Router
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Store Effects
  // -----------------------------------------------------------------------------------------------------

  fetchEvolutions$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_EVOLUTIONS_LIST_ACTION),
      mergeMap((action) => {
        let finalResult: any[] = [];
        let meta: IListMeta;

        return this.evolutionsService
          .fetchAllRawEvolutions(action.limit, action.offset)
          .pipe(
            mergeMap((list) => {
              const raw: any[] = list?.results;
              const { results, ...metaObject } = list;
              meta = metaObject;

              return forkJoin(
                raw.map((rawEvolution) => {
                  return this.evolutionsService.fetchEvolutionTree(rawEvolution.url).pipe(
                    mergeMap((evolutionTree) =>
                      of(this.getChain(evolutionTree)).pipe(
                        switchMap((chain) => {
                          let evolutionChain: any[] = [];

                          return forkJoin(
                            chain.map((chainItem: any) => {
                              const evoDetails = chainItem.evolution_details[0];
                              const splitUrl = chainItem.species.url.split('/');
                              const speciesId = splitUrl[splitUrl.length - 2];
                              const hasItem: boolean =
                                evoDetails && evoDetails.trigger.name === 'use-item' ? true : false;

                              const paddedId = `${speciesId}`.padStart(3, '0');

                              const evolItem: any = {
                                speciesName: chainItem.species.name,
                                speciesId,
                                img: `https://assets.pokemon.com/assets/cms2/img/pokedex/full/${paddedId}.png`,
                                minLevel: !evoDetails ? 1 : evoDetails.min_level,
                                triggerName: !evoDetails ? null : evoDetails.trigger.name
                              };

                              if (!hasItem) {
                                evolutionChain.push(evolItem);
                                return of(chainItem);
                              }

                              return this.evolutionsService.fetchEvolutionItem(evoDetails?.item?.url).pipe(
                                switchMap((lol) => {
                                  evolItem.item = lol;
                                  evolutionChain.push(evolItem);
                                  return chainItem;
                                }),
                                catchError((error) => of(error))
                              );
                            })
                          ).pipe(tap(() => finalResult.push(evolutionChain)));
                        })
                      )
                    ),
                    catchError((error) =>
                      this.handleError(error, 'Error', 'An error occured while fetching the evolutions')
                    )
                  );
                })
              );
            })
          )
          .pipe(
            map(() => FETCH_EVOLUTIONS_LIST_SUCCESS_ACTION({ evolutions: finalResult, meta })),
            catchError((error) => this.handleError(error, 'Error', 'An error occured while fetching the evolutions'))
          );
      })
    );
  });

  private handleError(error: HttpErrorResponse, title: string, message: string): Observable<any> {
    this.toasterService.error(title, message);
    this.router.navigateByUrl('');
    return of(FETCH_EVOLUTIONS_LIST_ERROR_ACTION({ error }));
  }

  private getChain(evolutionTree: any): any {
    const evolutionsChain = [];

    let evolutionItem = evolutionTree.chain;

    while (evolutionItem && evolutionItem.hasOwnProperty('evolves_to')) {
      evolutionsChain.push(evolutionItem);

      if (evolutionItem.evolves_to[0] !== undefined) {
        evolutionItem = evolutionItem.evolves_to[0];
      } else {
        break;
      }
    }

    return evolutionsChain;
  }
}
