export enum EvolutionsListActionTypes {
  FETCH_EVOLUTIONS_LIST = '[EVOLUTIONS LIST] Fetch evolutions list',
  FETCH_EVOLUTIONS_LIST_SUCCESS = '[EVOLUTIONS LIST] Fetch evolutions list Success',
  FETCH_EVOLUTIONS_LIST_ERROR = '[EVOLUTIONS LIST] Fetch evolutions list Error'
}
