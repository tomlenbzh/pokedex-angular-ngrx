import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { filter, map, Observable } from 'rxjs';
import { AppState } from 'src/app/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import { FETCH_EVOLUTIONS_LIST_ACTION } from './evolutions.actions';
import { selectEvolutions, selectEvolutionsLoading, selectEvolutionsMeta } from './evolutions.selectors';

@Injectable({ providedIn: 'root' })
export class EvolutionsHelper {
  constructor(private store: Store<AppState>) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Selectors
  // -----------------------------------------------------------------------------------------------------

  isLoading(): Observable<boolean> {
    return this.store.pipe(select(selectEvolutionsLoading));
  }

  evolutions(): Observable<any[] | null> {
    return this.store.select(selectEvolutions);
  }

  meta(): Observable<IListMeta | null> {
    return this.store.select(selectEvolutionsMeta);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Store Actions
  // -----------------------------------------------------------------------------------------------------

  /**
   * Fetches a list of Pokémons'evolutions.
   *
   * @param     { number }      limit     // The max number of items to be returned
   * @param     { number }      offset    // The first element of the page.
   */
  fetchEvolutions(limit: number, offset: number): void {
    this.store.dispatch(FETCH_EVOLUTIONS_LIST_ACTION({ limit, offset }));
  }
}
