import { createReducer, on } from '@ngrx/store';
import { IListMeta } from 'src/app/utils/models/list-meta.interface';
import {
  FETCH_EVOLUTIONS_LIST_ACTION,
  FETCH_EVOLUTIONS_LIST_SUCCESS_ACTION,
  FETCH_EVOLUTIONS_LIST_ERROR_ACTION
} from './evolutions.actions';

export interface EvolutionsState {
  evolutions: any[];
  meta: IListMeta | null;
  isLoading: boolean;
}

export const initialState: EvolutionsState = {
  evolutions: [],
  meta: null,
  isLoading: false
};

export const evolutionsListReducer = createReducer(
  initialState,
  on(FETCH_EVOLUTIONS_LIST_ACTION, (state: EvolutionsState): EvolutionsState => ({ ...state, isLoading: true })),
  on(FETCH_EVOLUTIONS_LIST_ERROR_ACTION, (state: EvolutionsState): EvolutionsState => ({ ...state, isLoading: false })),
  on(
    FETCH_EVOLUTIONS_LIST_SUCCESS_ACTION,
    (state: EvolutionsState, { evolutions, meta }): EvolutionsState => ({ evolutions, meta, isLoading: false })
  )
);
