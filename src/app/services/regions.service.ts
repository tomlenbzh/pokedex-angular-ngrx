import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class RegionsService {
  constructor(private httpClient: HttpClient) {}

  fetchAllRegions(): Observable<any> {
    return this.httpClient.get<any>(`${environment.baseUrl}/region`);
  }

  fetchRegion(region: string): Observable<any> {
    return this.httpClient.get<any>(`${environment.baseUrl}/region/${region}`);
  }
}
