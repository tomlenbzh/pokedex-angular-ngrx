import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { buildQueryParams } from '../utils/methods/build-query-params';

@Injectable({ providedIn: 'root' })
export class PokemonsService {
  private baseUrl = `${environment.baseUrl}/pokemon`;

  constructor(private httpClient: HttpClient) {}

  fetchAllPokemons(limit?: number): Observable<any> {
    const queryParams = { limit: 1200 };
    const url = `${this.baseUrl}${buildQueryParams(queryParams)}`;

    return this.httpClient.get<any>(url);
  }

  fetchPokemonsList(limit?: number, offset?: number): Observable<any> {
    const queryParams = { limit, offset };
    const url = `${this.baseUrl}${buildQueryParams(queryParams)}`;

    return this.httpClient.get<any>(url);
  }

  fetchPokemonInformation(pokemon: any): Observable<any> {
    return this.httpClient.get<any>(pokemon.url);
  }

  fetchPokemonDetailById(pokemonId: any): Observable<any> {
    return this.httpClient.get<any>(`${this.baseUrl}/${pokemonId}`);
  }

  fetchPokemonAbility(url: string): Observable<any> {
    return this.httpClient.get<any>(url);
  }

  fetchPokemonMove(url: string): Observable<any> {
    return this.httpClient.get<any>(url);
  }
}
