import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { buildQueryParams } from '../utils/methods/build-query-params';

@Injectable({ providedIn: 'root' })
export class EvolutionsService {
  constructor(private httpClient: HttpClient) {}

  fetchAllRawEvolutions(limit?: number, offset?: number): Observable<any> {
    const queryParams = { limit, offset };
    const url = `${environment.baseUrl}/evolution-chain${buildQueryParams(queryParams)}`;

    return this.httpClient.get<any>(url);
  }

  fetchEvolutionTree(url: string): Observable<any> {
    return this.httpClient.get<any>(url);
  }

  fetchEvolutionItem(url: string): Observable<any> {
    return this.httpClient.get<any>(url);
  }
}
